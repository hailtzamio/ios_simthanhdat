//
//  Configs.swift
//  FastShare
//
//  Created by Fullname on 1/18/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import Foundation

struct Production {
    
    
//    static let BASE_URL: String = "http://api.giaido.vn/api/"
      static let BASE_URL: String = "http://27.118.22.28:9125/api/"
    static var  ACCESS_TOKEN: String = ""
     static var  USER_ROLE: String = ""
}

enum NetworkErrorType {
    case API_ERROR
    case HTTP_ERROR
}

struct Parameter {
    // headers
    static let Accept = "Accept"
    static let ApplicationJson = "application/json"
    static let AccessTokenType = "access-token"
    static let ContentType = "Content-Type"
    static let FormUrlEncoded = "application/x-www-form-urlencoded"
    static let MultiPartFormData = "multipart/form-data"
}
