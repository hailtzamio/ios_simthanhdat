//
//  APIRouter.swift
//  FastShare
//
//  Created by Fullname on 1/18/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import Foundation
import Alamofire

enum APIRouter: URLRequestConvertible {
    // =========== Begin define api ===========
    case login(username: String, password: String)
    case changePassword(pass: String, newPass: String, confirmNewPass: String)
    case loginWithFacebook(facebook_id:String)
    case register(fullname:String, password:String, email:String, phone:String)
    case updateProfile(fullname:String, emai:String,avatar:String)
    case searchSim(telcoId:String, categoryId:String,pattern:String, priceFrom:String,priceTo:String, page:Int,number:String,birthYear:String, notContain4:Bool,notContain7:Bool,sort:String)
    
    case getInvoice()
    case getAddress()
    case getDistrict(provinceId:String)
    case getWard(districtId:String)
    case getSimCaterogy()
    case holdSim(simId:String)
    case myCart(page:Int)
    case payCart(payCart:PayCart)
    case myProfile()
    case addNewCart(simId:CLong, customerName:String, customerPhone:String, customerAddress:String)
    case getCartById(carId:CLong)
    case getSimOrderAgency(status:String)
    case approveOrderByAgency(orderId:CLong)
    case cancelOrderByAgency(orderId:CLong)
    
    
    // =========== End define api ===========
    
    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        switch self {
        case .login, .changePassword,.loginWithFacebook, .updateProfile, .register, .holdSim, .payCart, .addNewCart, .approveOrderByAgency :
            return .post
        case .searchSim, .getSimCaterogy, .getAddress, .getDistrict, .getInvoice, .myCart, .myProfile, .getWard, .getCartById, .getSimOrderAgency, .cancelOrderByAgency:
            return .get
            
        }
    }
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .login:
            return "oauth/token"
        case .loginWithFacebook:
            return "user/login-by-google"
        case .changePassword:
            return "v1/user/change_password"
        case .register:
            return "register"
        case .searchSim:
            return "search"
        case .updateProfile:
            return "update-profile"
        case .getSimCaterogy:
            return "category"
        case .holdSim:
            return "affiliate/add-to-cart"
        case .getAddress:
            return "province"
        case .getDistrict:
            return "district"
        case .getInvoice:
            return "affiliate/my-reservations"
        case .myCart:
            return "affiliate/carts"
        case .payCart:
            return "affiliate/pay-cart"
        case .myProfile:
            return "my-profile"
        case .getWard:
            return "ward"
        case .addNewCart:
            return "affiliate/new-cart"
        case .getCartById:
            return "affiliate/cart"
        case .getSimOrderAgency:
            
             return "agency/orders"
        case .approveOrderByAgency:
            return "agency/approve-order"
        case .cancelOrderByAgency:
            return "agency/cancel-order"
        }
    }
    
    // MARK: - Headers
    private var headers: HTTPHeaders {
        var headers = ["Content-Type": "application/json"]
        switch self {
        case .login:
            //            headers["Authorization"] = "Basic YXBwMToxMjM0NTY="
            break
        case .loginWithFacebook:
            break
            
        case .register:
            
            break
        case .changePassword, .searchSim, .updateProfile, .getSimCaterogy, .holdSim, .getAddress, .getDistrict, .getInvoice, .myCart, .myProfile, .getWard, .payCart, .addNewCart, .getCartById, .getSimOrderAgency, .approveOrderByAgency, .cancelOrderByAgency:
            headers["Authorization"] = getAuthorizationHeader()
            break
        }
        
        return headers;
    }
    
    // MARK: - Parameters
    private var parameters: Parameters? {
        switch self {
        case .login(let username, let password):
            return [
                "username": username,
                "password": password,
                "grant_type": "grant_type"
            ]
            
        case .loginWithFacebook(let facebook_id):
            return ["google_id":facebook_id]
        case .changePassword(let pass, let newPass, let confirmNewPass):
            return[
                "password": pass,
                "new_password": newPass,
                "new_password_confirmation": confirmNewPass
            ]
            
        case .register(let fullName,let password,let email,let phone):
            return [
                
                "fullName": fullName,
                "password": password,
                "email": email,
                "phone": phone
            ]
            
        case .searchSim(let telcoId, let categoryId, let pattern, let priceFrom, let priceTo, let page,let number, let birthYear, let notContain4,let notContain7, let sort):
            return [
                "pattern":pattern,
                "telcoId": telcoId,
                "categoryId": categoryId,
                "page": page,
                "priceFrom": priceFrom,
                "priceTo":priceTo,
                "notContain4":notContain4,
                "notContain7":notContain7,
                "number":number,
                "birthYear":birthYear,
                "sort":sort,
                "status":0
            ]
            
        case .updateProfile(let fullName,let email,let avatar):
            return [
                "fullName": fullName,
                "password": email,
                "email": avatar,
                "phone": "0912999999"
            ]
            
        case .getAddress:
            
            return [:]
            
        case .getDistrict(let provinceId) :
            
            return [
                "size":100,
                "provinceId": provinceId,
            ]
            
        case .getSimCaterogy :
            return [:]
        case .holdSim(let simId):
            return [
                "simId":simId
            ]
            
        case .getInvoice():
            return [:]
            
        case .myCart(let page):
            return [
                "size":100,
                "page": page,
            ]
            
        case .myProfile():
            return [:]
            
        case .getWard(let districtId):
            return [
                "size":100,
                "page": 0,
                "districtId":districtId
            ]
        case .payCart(let payCart):
            return [
                "cartId": payCart.cartId,
                "customerName": payCart.customerName,
                "customerPhone": payCart.customerPhone,
                "customerAddress": payCart.customerAddress,
                "provinceId": payCart.provinceId,
                "districtId": payCart.districtId,
                "wardId": payCart.wardId
            ]
        case .addNewCart(let simId, let customerName, let customerPhone, let customerAddress):
            return [
                "simId": simId,
                "customerName": customerName,
                "customerPhone": customerPhone,
                "customerAddress": customerAddress
            ]
            
        case .getCartById(let carId):
            return [
                "id": carId
            ]
            
        case .getSimOrderAgency(let status):
            return [
                "status":status
            ]
            
        case .approveOrderByAgency(let orderId):
            return [
                "orderId":orderId
            ]
            
        case .cancelOrderByAgency(let orderId):
            return [
                "orderId":orderId
            ]
            
        }
        
    }
    
    // MARK: - URL request
    func asURLRequest() throws -> URLRequest {
        let url = try Production.BASE_URL.asURL()
        
        // setting path
        var urlRequest: URLRequest = URLRequest(url: url.appendingPathComponent(path))
        
        urlRequest.timeoutInterval = 10
        
        // setting method
        urlRequest.httpMethod = method.rawValue
        
        // setting header
        for (key, value) in headers {
            urlRequest.addValue(value, forHTTPHeaderField: key)
        }
        
        if let parameters = parameters {
            do {
                urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
            } catch {
                print("Encoding fail")
            }
        }
        
        return urlRequest
    }
    
    private func getAuthorizationHeader() -> String? {
        return "Bearer " + Production.ACCESS_TOKEN
    }
}
