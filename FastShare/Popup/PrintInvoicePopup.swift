//
//  PrintInvoicePopup.swift
//  FastShare
//
//  Created by Tcsytems on 6/21/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import IQDropDownTextField
class PrintInvoicePopup: PopupView  {

    
    var action1: (() -> ())?
    var action2: (() -> ())?
      var paySuccessfully: (() -> ())?
      var payFail: (() -> ())?
    @IBOutlet weak var lbPhoneNumber: UILabel!
    
    @IBOutlet weak var lbPrice: UILabel!
    
    @IBOutlet weak var tfCity: IQDropDownTextField!
    
    @IBOutlet weak var tfProvince: IQDropDownTextField!
    
    
    @IBOutlet var tfWards: IQDropDownTextField!
    
    @IBOutlet var tfWard: IQDropDownTextField!
    
    @IBOutlet weak var tfAddress: UITextField!
    
    @IBOutlet var tfName: UITextField!
    
    
    @IBOutlet var tfPhone: UITextField!
    
    var cities = [Address]()
    var citiesString = [String]()
    
    var districts = [District]()
    var districtString = [""]
    
    var wards = [Address]()
    var wardString = [String]()
    
    var simId = ""
    
    var provinceId = 0
    var districtId = 0
    var wardId = 0
    var cartId = 0
    let viewModel = SearchViewModel()
    override func awakeFromNib() {
        

        tfProvince.isOptionalDropDown = true
        tfProvince.delegate =  self
        tfProvince.tag = 2
        
        viewModel.getDataSucessfully = { [weak self]  in
            self?.districtString.removeAll()
            if let districts = self?.viewModel.districts {
                districtsCont = districts
                for i in 0..<districts.count {
                    self?.districtString.append((districts[i].name)!)
                }
                self?.tfProvince.itemList = self?.districtString
            }
            
            
            if let wards = self?.viewModel.wards {
                self?.wards = wards
                for i in 0..<wards.count {
                    self?.wardString.append((wards[i].name)!)
                }
//                self?.tfWards.itemList = self?.wardString
            }
            
            
            
            
            
            if let invoice = self?.viewModel.invoice {
                
                print("OKKKKKKKKKKKKKKKKKK")
                
            }
        }
    }
    
    class func instanceFromNib(cities:[Address], cartId:Int) -> PrintInvoicePopup {
        let detailView = UINib(nibName: "PrintInvoicePopup", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PrintInvoicePopup

        detailView.animationType =  AnimationType.alpha
        detailView.popupFrame = CGRect.init(x: 20, y: (SCREEN_HEIGHT - 400)/2 , width: SCREEN_WIDTH - 40, height: 400)
        detailView.layer.cornerRadius = 5
        detailView.clipsToBounds = true

        detailView.cartId = cartId
        
        for i in 0..<cities.count {
            detailView.citiesString.append(cities[i].name!)
        }
        detailView.cities.removeAll()
        detailView.cities = cities
        detailView.tfCity.isOptionalDropDown = true
        detailView.tfCity.delegate =  detailView.self
        detailView.tfCity.itemList = detailView.citiesString
        detailView.tfCity.tag = 1
        
        return detailView
        
    }
    
    
    @IBAction func btnKeepSim(_ sender: Any) {
       
        if let action = action1 {
            action()
        }
        
        
        
        let payCart = PayCart()

//        if tfName != nil && tfName.text != nil {
            payCart.customerName = "Hai le"
//        }
//        if tfPhone != nil, tfPhone.text != nil {
          payCart.customerPhone = "0912 333 333"
          payCart.customerAddress = " Test Hanoi"
//        }
//
//        payCart.provinceId = provinceId
//        payCart.districtId = districtId
//        payCart.wardId = 1
        
   
        
        viewModel.payCart(payCart: payCart, completion : {(response) in
            if let paySuccessfully = self.paySuccessfully {
                paySuccessfully()
            }
        }){ (error) in
            if let payFail = self.payFail {
                payFail()
            }
        }

         self.hide()
    }
    
    
    @IBAction func btnCancel(_ sender: Any) {
        self.hide()
        if let action = action2 {
            action()
        }
    }
    
    
}


extension PrintInvoicePopup : IQDropDownTextFieldDelegate {
    func textField(_ textField: IQDropDownTextField, didSelectItem item: String?) {
        
        if textField.tag == 1 {
            
            for i in 0..<cities.count {
                if cities[i].name == item {
                     provinceId = cities[i].id!
                    viewModel.getDistrict(provinceId: String(cities[i].id!))
                   
                }
            }
        }
        
        if textField.tag == 2 {
            for i in 0..<districtsCont.count {
                if districtsCont[i].name == item {
                    districtId = districtsCont[i].id!
                    viewModel.getWard(districtId: String(districtsCont[i].id!))
                }
            }
        }
    
    }
    
    
}


