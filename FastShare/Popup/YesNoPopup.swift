//
//  YesNoPopup.swift
//  FastShare
//
//  Created by Tcsytems on 7/6/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import IQDropDownTextField
import IQKeyboardManagerSwift
import Toaster
class YesNoPopup: PopupView {
    
    var ok: (() -> ())?
    var cancel: (() -> ())?
    var close: (() -> ())?
  
    
  
    
    @IBOutlet weak var lbTitle: UILabel!
    
  
    
 
    override func awakeFromNib() {
        
        
    }
    
    class func instanceFromNib(title:String) -> YesNoPopup {
        let detailView = UINib(nibName: "YesNoPopup", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! YesNoPopup
        detailView.lbTitle.text = title
    
        
        
        detailView.animationType =  AnimationType.alpha
        detailView.popupFrame = CGRect.init(x: 20, y: (SCREEN_HEIGHT - 180)/2 , width: SCREEN_WIDTH - 40, height: 180)
        detailView.layer.cornerRadius = 5
        detailView.clipsToBounds = true
        
    
        
        return detailView
        
    }
    
    
    @IBAction func btnOk(_ sender: Any) {
        self.hide()
        if let action = ok {
            action()
        }

        
    }
    
    
    @IBAction func btnCancel(_ sender: Any) {
        self.hide()
        if let action = cancel {
            action()
        }
    }
    
    
    @IBAction func btnClose(_ sender: Any) {
        self.hide()
    }
}
