//
//  SimFilterPopup.swift
//  FastShare
//
//  Created by Tcsytems on 5/28/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import IQDropDownTextField
import IQKeyboardManagerSwift
import Toaster
class SimFilterPopup: PopupView
  {

    var action1: (() -> ())?
    var action2: ((SimSearch) -> ())?
    var categoryId = 0
    var telcoId = ""
    var isNotAllowed4 = false
    var isNotAllowed7 = false
    @IBOutlet weak var tfNiceSim: IQDropDownTextField!
    
    
    @IBOutlet weak var tfPriceFrom: UITextField!
    
    @IBOutlet weak var tfPriceTo: UITextField!
    var grounds = ["Tất cả","Viettel","Mobile", "Vina"]
     var telcoIds = ["","1", "2","3"]
    var niceSims = [String]()
    override func awakeFromNib() {
        IQKeyboardManager.shared.enable = true
        niceSims.append("Tất cả")
        for i in 0..<simCategories.count {
           niceSims.append(simCategories[i].name!)
        }
        
        tfPhone.isOptionalDropDown = true
        tfPhone.delegate =  self
        tfPhone.itemList = grounds
        tfPhone.tag = 1
        
        tfNiceSim.isOptionalDropDown = true
        tfNiceSim.delegate =  self
        tfNiceSim.itemList = niceSims
        tfNiceSim.tag = 2
        

        tfPhone.setSelectedItem("Viettel", animated: true)
         tfNiceSim.setSelectedItem(niceSims[1], animated: true)
      
    }

    @IBOutlet weak var tfPhone: IQDropDownTextField!
    class func instanceFromNib() -> SimFilterPopup {
        let detailView = UINib(nibName: "SimFilterPopup", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SimFilterPopup
//        detailView.lbPhoneNumber.text = sim.alias
//        if let price = sim.inputPrice {
//            detailView.lbPrice.text = "\(price)"
//        }
        
        detailView.animationType =  AnimationType.upDown
        detailView.popupFrame = CGRect.init(x: 35, y: (SCREEN_HEIGHT - 420)/2 , width: SCREEN_WIDTH - 70, height: 420)
        detailView.layer.cornerRadius = 10
        detailView.clipsToBounds = true
        return detailView
        
    }
    
    
    @IBAction func swNotAllowed4(_ sender: Any) {
        
        if ((sender as AnyObject).isOn == true){
            isNotAllowed4 = true
        }
        else{
            isNotAllowed4 = false
        }
    }
    
    
    @IBAction func swNotAllowed7(_ sender: Any) {
        if ((sender as AnyObject).isOn == true){
            isNotAllowed7 = true
        }
        else{
            isNotAllowed7 = false
        }
        
    }
    
    
    
    @IBAction func btnKeepSim(_ sender: Any) {
        self.hide()
        if let action = action1 {
            action()
        }
        
    }
    
    @IBAction func btnSearch(_ sender: Any) {
       
        
       
        
//        if telcoId == "" {
//            Toast(text: "Chọn loại mạng...", duration: Delay.short).show()
//            return
//        }
//        
//        if categoryId == 0{
//            Toast(text: "Chọn loại Sim đẹp...", duration: Delay.short).show()
//            return
//        }
    
        
        let simSearch = SimSearch()
        simSearch.categoryId = categoryId
        simSearch.telecom = telcoId
        simSearch.notAllow4 = isNotAllowed4
        simSearch.notAllow7 = isNotAllowed7
        simSearch.priceFrom = tfPriceFrom.text
        simSearch.priceTo = tfPriceTo.text
        
        if let action = action2 {
            action(simSearch)
        }
        
         self.hide()
    }
    
    
    @IBAction func btnClose(_ sender: Any) {
         self.hide()
    }
    
}

extension SimFilterPopup : IQDropDownTextFieldDelegate {
  func textField(_ textField: IQDropDownTextField, didSelectItem item: String?) {
   
    
    
    
  
    switch textField.tag {
    case 1:
        
        for i in 0..<telcoIds.count {
            if grounds[i] == item {
                telcoId = telcoIds[i]
            }
        }
          print(" =====> telcoId \(telcoId)")
        break
        
    case 2:
        
        for i in 0..<simCategories.count {
            if simCategories[i].name == item {
                categoryId = simCategories[i].id!
            }
        }
        
        print(" =====> \(categoryId)")
        break
    default:
        break
    }
    
    
    }
}
