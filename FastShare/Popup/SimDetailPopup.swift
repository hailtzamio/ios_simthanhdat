//
//  SimDetailPopup.swift
//  FastShare
//
//  Created by Tcsytems on 5/27/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import IQDropDownTextField
import JGProgressHUD
import IQKeyboardManagerSwift
class SimDetailPopup: PopupView {
    
    var action1: (() -> ())?
    var action2: (() -> ())?
     var holdSimSuccessfully: (() -> ())?
     var holdSimFail: (() -> ())?
    
    @IBOutlet weak var lbPhoneNumber: UILabel!
    
    @IBOutlet weak var lbPrice: UILabel!
    
    @IBOutlet var lfTele: UILabel!
    var cities = [Address]()
    var citiesString = [""]
    
    var district = [""]
    var simId = ""
    
    
    let viewModel = SearchViewModel()
    override func awakeFromNib() {
        
        
    }
    
    class func instanceFromNib(simId:Int,sim: Sim, image : String, cities:[Address]) -> SimDetailPopup {
        let detailView = UINib(nibName: "SimDetailPopup", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SimDetailPopup
        detailView.lbPhoneNumber.text = sim.alias
        if let price = sim.websitePrice {
            detailView.lbPrice.text = "\(price)"
        }
        
        if sim.telco != nil && sim.telco?.name != nil  {
            detailView.lfTele.text = sim.telco?.name
        }
        
        
        detailView.animationType =  AnimationType.alpha
        detailView.popupFrame = CGRect.init(x: 20, y: (SCREEN_HEIGHT - 225)/2 , width: SCREEN_WIDTH - 40, height: 225)
        detailView.layer.cornerRadius = 5
        detailView.clipsToBounds = true
        
        detailView.simId = String(simId)
        
        for i in 0..<cities.count {
            detailView.citiesString.append(cities[i].name!)
        }
        
        return detailView
        
    }
    
    
    @IBAction func btnKeepSim(_ sender: Any) {
        self.hide()
        if let action = action1 {
            action()
        }
        
        
      
        
        
//        viewModel.holdSim(simId: simId)
        
        
    }
    
    
    @IBAction func btnCancel(_ sender: Any) {
        self.hide()
        if let action = action2 {
            action()
        }
    }
    
    
    @IBAction func btnClose(_ sender: Any) {
              self.hide()
    }
}

