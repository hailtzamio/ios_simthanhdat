//
//  BaseViewController.swift
//  FastShare
//
//  Created by Fullname on 1/17/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import Toaster
import JGProgressHUD
class BaseViewController: UIViewController {
    
    let hud = JGProgressHUD(style: .dark)
  
   let baseViewModel = BaseViewModel()
    let preferences = UserDefaults.standard
    let indicator: UIActivityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
//        hud.textLabel.text = "Loading"
        
        baseViewModel.checkInternet = { [weak self] in
            self?.hud.dismiss(afterDelay: 0.1)
            self?.toastShort(content: "Check Internet ")
        }
    }
    
    func showLoading()  {
        hud.show(in: self.view)
    }
    
    func stopLoading()  {
         hud.dismiss(afterDelay: 0.0)
    }
    
    
    func toastShort(content:String){
        Toast(text: content, duration: Delay.short).show()
    }
    
    func swipeToPop() {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true;
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;
    }
    
    func showAlert(message: String,
                   title: String? = "",
                   cancel: String = "Cancel",
                   otherButtons: [String] = [],
                   action: ((String) -> Void)? = nil) {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertVC.addAction(UIAlertAction(title: cancel, style: .cancel, handler: { _ in
            action?(cancel)
        }))
        
        otherButtons.forEach { (title) in
            alertVC.addAction(UIAlertAction(title: title, style: .default, handler: { (alertAction) in
                action?(alertAction.title ?? "")
            }))
        }
        self.present(alertVC, animated: true, completion: nil)
    }
    
}
