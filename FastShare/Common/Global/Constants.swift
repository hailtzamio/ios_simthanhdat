//
//  Constants.swift
//  FastShare
//
//  Created by Fullname on 1/17/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit

let SCREEN_WIDTH = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height

let all = ""
let viettel = "1"
let vina = "2"
let mobile = "3"
let nonTelecom = -1


enum ControllerId: Int {
    case CartListViewController = 1
    case CreateNewCartViewController = 2
}

let access_token_key = "access_token_key"
let number_phone_key = "number_phone_key"
let passwork_key = "passwork_key"
let user_role_key = "user_role_key"

let error_check_internet = "Error Internet"
var simCategories = [SimCategory]()
var districtsCont = [District]()
enum ControllerID : Int {
    case Login = 0
    case Main = 1
    case Register = 2
    case Search
    case ForgotPassword
    case ChangePassword
    case Verify
    case UserInformation
    case EditUserInformation
    case RequestAssistance
    case Settings
    case RequestDetail
    case Waiting
    case serviceProvider
    case serviceList
    case serviceDetail
    case remarkAndRate
    case cancellation
}

enum ProfileControllerID : Int {
    case UpdateProfile
    case ChangePassword
}

enum SimStatus : Int {
    case new
    case processing
    case done
}

enum Telecom : Int {
    case viettel = 1
    case mobile = 3
    case vina = 2
}

struct UserDefaultsKey {
    static let AccessToken = "AccessToken"
    static let UserLogin = "UserLogin"
    static let IndexIntro = "IndexIntro"
     static let SimCategory = "SimCategory"
}

struct SearchSimType {
    static let Telecom = "Telecom"
    static let Price = "Price"
    static let NiceNumber = "NiceNumber"
}



func getControllerFromProfile(id:ProfileControllerID) -> UIViewController {
    var newController : UIViewController
    let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
    switch id {
    case .UpdateProfile:
        newController = storyboard.instantiateViewController(withIdentifier: "UpdateProfileViewController")
    case .ChangePassword :
        newController = storyboard.instantiateViewController(withIdentifier: "ChangePasswordViewController")
    }
    
    return newController
}

func getControllerFromHome (id : ControllerID) -> UIViewController {
    var newController : UIViewController
    let storyboard = UIStoryboard.init(name: "Home", bundle: nil)
    
    switch id {
    case .Login:
        newController = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
    case .Main :
        newController = storyboard.instantiateViewController(withIdentifier: "HomeTabBarViewController")
    case .Register:
        newController = storyboard.instantiateViewController(withIdentifier: "registerController") as! RegisterViewController
    case .Search:
        newController = storyboard.instantiateViewController(withIdentifier: "searhViewController") as! SearhViewController
    case .ForgotPassword:
        newController = storyboard.instantiateViewController(withIdentifier: "forgotpassController")
    case .ChangePassword:
        newController = storyboard.instantiateViewController(withIdentifier: "changepassController")

    case .Verify:
        newController = storyboard.instantiateViewController(withIdentifier: "verifyController")
    case .UserInformation:
        newController = storyboard.instantiateViewController(withIdentifier: "userdetailController")
    case .EditUserInformation:
        newController = storyboard.instantiateViewController(withIdentifier: "editInfoController")
    case .RequestAssistance:
        newController = storyboard.instantiateViewController(withIdentifier: "requestAssistanceController")
    case .Settings:
        newController = storyboard.instantiateViewController(withIdentifier: "settingController")
    case .RequestDetail:
        newController = storyboard.instantiateViewController(withIdentifier: "requestDetail")
    case .Waiting:
        newController = storyboard.instantiateViewController(withIdentifier: "waiting")
    case .serviceProvider:
        newController = storyboard.instantiateViewController(withIdentifier: "doneRequest")
    case .serviceList:
        newController = storyboard.instantiateViewController(withIdentifier: "servicelist")
    case .serviceDetail:
        newController = storyboard.instantiateViewController(withIdentifier: "serviceDetail")
    case .remarkAndRate:
        newController = storyboard.instantiateViewController(withIdentifier: "rating")
    case .cancellation:
        newController = storyboard.instantiateViewController(withIdentifier: "canellation")
    }
    
    return newController
}
