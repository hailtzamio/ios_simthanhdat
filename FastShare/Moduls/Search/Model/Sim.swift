//
//  Sim.swift
//  FastShare
//
//  Created by Tcsytems on 5/23/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import ObjectMapper


class Sim:Mappable  {
    
    
    var id:Int?
    var simId:CLong?
    var number: String?
    var alias: String?
    var telco:Telco?
    var category:Category?
    var inputPrice: CLong?
    var collaboratorPrice:CLong?
    var websitePrice:CLong?
    var isSelected:Bool = false
    var status:Int?
    var createdAt:String?
    var agencyResponse:String?
    var cateName:String?
    
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map)
    {
        id <- map["id"]
        number <- map["number"]
        alias <- map["alias"]
        telco <- map["telco"]
        category <- map["category"]
        inputPrice <- map["inputPrice"]
        collaboratorPrice <- map["collaboratorPrice"]
        websitePrice <- map["websitePrice"]
         status <- map["status"]
         simId <- map["simId"]
        agencyResponse <- map["agencyResponse"]
         createdAt <- map["createdAt"]
         cateName <- map["cateName"]
    }

}

class Telco:Mappable {
    var id:CLong?
    var name: String?
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map)
    {
        id <- map["id"]
        name <- map["name"]
        
    }
}

class Category:Mappable {
    var id:CLong?
    var name: String?
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map)
    {
        id <- map["id"]
        name <- map["name"]
        
    }
}
