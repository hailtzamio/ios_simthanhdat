//
//  District.swift
//  FastShare
//
//  Created by Tcsytems on 6/20/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import ObjectMapper
class District:Mappable  {
    
    
    var id:Int?
    var name: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map)
    {
        id <- map["id"]
        name <- map["name"]
    }
    
}
