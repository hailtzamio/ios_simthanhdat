//
//  SimReq.swift
//  FastShare
//
//  Created by Tcsytems on 6/26/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import ObjectMapper
class SimReq:Mappable {
   
    var simId: String?
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map)
    {
      
        simId <- map["simId"]
        
    }
}
