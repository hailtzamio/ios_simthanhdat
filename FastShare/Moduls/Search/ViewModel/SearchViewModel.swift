//
//  SearchViewModel.swift
//  FastShare
//
//  Created by Tcsytems on 5/23/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ObjectMapper
class SearchViewModel: BaseViewModel {
    
    
     var getSimListSucessfully: (() -> ())?
    var totalSim: ((String) -> ())?
    
    var data:[Sim]? {
        didSet {
            guard data != nil else { return }
            self.getSimListSucessfully?()
        }
    }
    
    var invoice:Invoice? {
        didSet {
            guard invoice != nil else { return }
            self.getDataSucessfully?()
        }
    }
    
    var address:[Address]? {
        didSet {
            guard address != nil else { return }
            self.getDataSucessfully?()
        }
    }
    
    var districts:[District]? {
        didSet {
            guard districts != nil else { return }
            self.getDataSucessfully?()
        }
    }
    
    var wards:[Address]? {
        didSet {
            guard wards != nil else { return }
            self.getDataSucessfully?()
        }
    }
    
    func searchSim(with telcoId:String, categoryId:String,pattern:String, priceFrom:String,priceTo:String, page:Int,number:String,birthYear:String,notContain4:Bool,notContain7:Bool, sort:String){
        
        MGConnection.requestDataList(APIRouter.searchSim(telcoId: telcoId, categoryId: categoryId,pattern:pattern, priceFrom:priceFrom, priceTo:priceTo, page: page,number:number,birthYear: birthYear,notContain4: notContain4,notContain7:notContain7,sort:sort), Sim.self) { (result, error) in
            
            if(error?.mErrorCode == 40){
                self.checkInternet?()
                return
            }
            
            guard error == nil else {
                self.isLoading = false
                return
            }
            
            if let listSim = result {
                self.data = listSim
                return
            }
        }
        
    }
    
    func getAddress(){
        MGConnection.requestDataList(APIRouter.getAddress(), Address.self) { (result, error) in
            
            if(error?.mErrorCode == 40){
                self.checkInternet?()
                return
            }
            
            guard error == nil else {
                self.isLoading = false
                return
            }
            
            if let address = result {
                self.address = address
                return
            }
        }
    }
    
    
    
    func getDistrict(provinceId:String){
        MGConnection.requestDataList(APIRouter.getDistrict(provinceId: provinceId), District.self) { (result, error) in
            
            if(error?.mErrorCode == 40){
                self.checkInternet?()
                return
            }
            
            guard error == nil else {
                self.isLoading = false
                return
            }
            
            if let district = result {
                self.districts = district
                return
            }
        }
    }
    
    func getWard(districtId:String){
        MGConnection.requestDataList(APIRouter.getWard(districtId: districtId), Address.self) { (result, error) in
            
            if(error?.mErrorCode == 40){
                self.checkInternet?()
                return
            }
            
            guard error == nil else {
                self.isLoading = false
                return
            }
            
            if let district = result {
                self.wards = district
                return
            }
        }
    }
    
    func payCart( payCart:PayCart, completion : @escaping (_ response: ServerResponse) -> (), fail: @escaping (Error) -> Void) -> Void {
        
        let parameters = [
            "customerName": payCart.customerName,
            "customerPhone": payCart.customerPhone,
            "customerAddress": payCart.customerAddress
        ]
        let url = "http://27.118.22.28:9125/api/affiliate/pay-cart"
        ServiceFactory.post(url: url, parameters: parameters) { response in
            if response.result.isSuccess, response.response?.statusCode == 200 {
                if let serverResponse = Mapper<ServerResponse>().map(JSONObject: response.result.value) {
                    completion(serverResponse)
                    
                    let jsonResponse = response.result.value as! NSDictionary
                    if jsonResponse["status"] != nil {
                        
                        print("Okie")
                    }
                }
            } else {
                if let error = response.result.error {
                    fail(error)
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: nil)
                    fail(error)
                }
            }
        }
    }
    
    
    
    func payCart(payCart:PayCart){
        
        MGConnection.request(APIRouter.payCart(payCart:payCart), Invoice.self) { (result, error) in
            
            if(error?.mErrorCode == 40){
                self.checkInternet?()
                return
            }
            
            guard error == nil else {
                self.isLoading = false
                return
            }
            
            if let invoice = result {
                self.invoice = invoice
                return
            }
        }
        
    }
    
    
    
    func holdSim(simId:String){
        
        MGConnection.request(APIRouter.holdSim(simId:simId), Invoice.self) { (result, error) in
            
            if(error?.mErrorCode == 40){
                self.checkInternet?()
                return
            }
            
            guard error == nil else {
                self.isLoading = false
                return
            }
            
            if let invoice = result {
                self.invoice = invoice
                return
            }
        }
        
    }
    
   
    
    
    func makeInvoiceTest(simId:String, customerName:String,customerAddress:String, customerPhone:String, provinceId:String, districtId:String, wardId:String){
        let url = "http://27.118.22.28:9125/api/sim/hold"
        var _headers : HTTPHeaders = ["Content-Type": "application/json"]
        _headers["Authorization"] = "Bearer " + Production.ACCESS_TOKEN
        let params : Parameters = ["simId":"7199","customerName":customerName,"customerAddress":customerAddress,"customerPhone":customerPhone,"provinceId":provinceId,"districtId":districtId,"wardId":wardId]
        
        request(url, method: .post, parameters: params, encoding: URLEncoding.default , headers: _headers).responseData(completionHandler: {
            response in response
            
            switch response.result {
            case .success:
                //                let jsonResponse = response.result.value as! NSDictionary
                //                if jsonResponse["data"] != nil {
                //
                //
                //                    print("Okie")
                //                }
                break
                
            case .failure(let error):
                if error is AFError {
                    self.isLoading = false
                }
                
                break
            }
            
            
        })
    }
}
