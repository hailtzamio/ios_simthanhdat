    //
    //  SearhViewController.swift
    //  FastShare
    //
    //  Created by Tcsytems on 4/28/19.
    //  Copyright © 2019 Fullname. All rights reserved.
    //
    
    import UIKit
    import IQKeyboardManagerSwift
    enum SimeSearchType : Int {
        case All
        case Telecom
        case Year
        case Price
        case NiceNumber
    }
    
    class SearhViewController: BaseViewController {
        
        
        
        @IBOutlet weak var btnVinaOutlet: UIButton!
        @IBOutlet weak var lbIntroduce: UIView!
        @IBOutlet weak var tfSearch: UITextField!
        
        @IBOutlet weak var totalSim: UILabel!
        @IBOutlet weak var lbAll: UILabel!
        
        @IBOutlet weak var tbViettel: UILabel!
        
        @IBOutlet weak var lbMobile: UILabel!
        
        @IBOutlet weak var lbVina: UILabel!
        @IBOutlet weak var lbTitl: UILabel!
        @IBOutlet weak var tbSim: UITableView!
        
        @IBOutlet var copyWord: UIButton!
        
        @IBOutlet weak var imvDataWarning: UIImageView!
        
        @IBOutlet var lbCopyNumberCount: UILabel!
        
        var telecoms = [UILabel]()
        var loadingData = false
        var isFilterPopupOpening = false
        // Param To Request Server
        var page = 0
        var pattern = ""
        var categoryId = ""
        var telecom = all
        var price = "0"
        var priceFrom = ""
        var priceTo = ""
        var notContain7 = false
        var notContain4 = false
        var number = ""
        var birthYear = ""
        var sort = "price+asc"
        
        
        var simSearchType:String? = nil
        var content:String = ""
        var searchTitle:String = ""
        var selectedSims = [Sim]()
        
        let viewModel = SearchViewModel()
        var sims = [Sim]()
        var cities = [Address]()
        
        

        
        override func viewDidLoad() {
            super.viewDidLoad()
            

            
            tfSearch.delegate = self
            tfSearch.addPadding(.left(45.0))
            tfSearch.keyboardType = UIKeyboardType.phonePad
            tfSearch.delegate = self
            
          
            telecoms.append(lbAll) // Viettel 1
            telecoms.append(tbViettel) // Mobile 3
            telecoms.append(lbMobile) // vina 2
            telecoms.append(lbVina) // nothing
            
            
            tbSim.dataSource = self
            tbSim.delegate = self
            
            tbSim?.register(SimTableViewCell.nib, forCellReuseIdentifier: SimTableViewCell.identifier)
            lbIntroduce.isHidden = false
            tbSim.isHidden = true
            
//            lbVina.isHidden = true
            //            lbTitl.text = searchTitle
            
            
            NotificationCenter.default.addObserver(self, selector: #selector(SearhViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
            
            
            
            
            switch simSearchType {
            case SearchSimType.Telecom:
                updateLableColor(pos: (Int(content) ?? 1))
                telecom = content
                break
            case SearchSimType.Price:
                let fullNameArr = content.components(separatedBy: "-")
                priceFrom    = fullNameArr[0]
                priceTo = fullNameArr[1]
                price = content
                updateLableColor(pos: 0)
            case SearchSimType.NiceNumber :
                categoryId = content
                updateLableColor(pos: 0)
            default:
                break
            }
            
            print("Find with \(content) \(simSearchType)")
            
            getSimList()
            viewModel.getAddress()
            
            tfSearch.addDoneOnKeyboardWithTarget(self, action: #selector(doneButtonClicked), titleText:"Nhập số cần tìm ...")
        }
        
        @objc func doneButtonClicked(_ sender: Any) {
            view.endEditing(true)
            
            page = 0
            if let searchText = tfSearch.text {
                if searchText.contains("*") {
                    pattern = searchText
                    birthYear = ""
                    number = ""
                } else {
                    if searchText.count == 4 {
                        birthYear = searchText
                        pattern = ""
                        number = ""
                    } else {
                        number = searchText
                        pattern = ""
                        birthYear = ""
                    }
                    
                }
                
                resetValueAfterMoveToAnotherTelecom()
                getSimList()
            }
        }
        
        @objc func keyboardWillHide(_ notification: NSNotification){
            
            print("isFilterPopupOpening with \(isFilterPopupOpening)")
            
        }
        
        func getSimList(){
            
            // remove Copy Count
            selectedSims.removeAll()
            checkToHideCopyButton()
            
            if(!loadingData){
                lbIntroduce.isHidden = false
                tbSim.isHidden = true
            } else {
                lbIntroduce.isHidden = true
                tbSim.isHidden = false
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.showLoading()
            }
            
            viewModel.checkInternet = { [weak self] in
                self?.hud.dismiss(afterDelay: 0.1)
                self?.toastShort(content: error_check_internet)
            }
            
            viewModel.getSimListSucessfully = { [weak self]  in
                if (self?.viewModel.data) != nil {
                    self?.hud.dismiss(afterDelay: 0.05)
                    self?.sims.append(contentsOf: (self?.viewModel.data)!)
                    
                    self?.lbIntroduce.isHidden = true
                  
                    self?.tbSim.reloadData()
                    self?.loadingData = false
                    
                    if (self?.sims.count)! > 0 {
                        self?.tbSim.isHidden = false
                        self?.imvDataWarning.isHidden = true
                        self?.page = (self?.page)! + 1
                    } else {
                          self?.tbSim.isHidden = true
                         self?.imvDataWarning.isHidden = false
                    }
                    
               
                    
                    
                    if let v = self?.viewModel.data?.count {
                        self?.totalSim.text = "\(v) Sim"
                    }
                    
                    
                }
            }
            
            viewModel.getDataSucessfully = { [weak self]  in                if let cities = self?.viewModel.address {
                self?.cities = cities
                }
            }
            
            //            viewModel.totalSim = { total in
            //
            //                self.totalSim.text = total
            //            }
            
            viewModel.getDataFail = { [weak self] in
                self?.stopLoading()
                self?.toastShort(content: "Wrong")
            }
            
            
            let sims = SimCategory.getSimCategory()
            print("Total Sim = \(sims.count)")
            
            viewModel.searchSim(with: telecom, categoryId: categoryId,pattern:pattern, priceFrom:priceFrom,priceTo: priceTo, page: page,number:number,birthYear:birthYear,notContain4:notContain4,notContain7:notContain7,sort:sort)
            
        }
        
        
        func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
            if (text == "\n") {
                textView.resignFirstResponder()
            }
            return true
        }
        
        func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            //                        let lastElement = sims.count - 1
            //                        if !loadingData && indexPath.row == lastElement {
            ////                            indicator.startAnimating()
            //                            page = page + 1
            //                            loadingData = true
            //                            getSimList()
            //
            //                        }
        }
        
        
        
        @IBAction func btnBack(_ sender: Any) {
            navigationController?.popViewController(animated: false)
            //            self.willMove(toParent: nil)
            //            self.view.removeFromSuperview()
            //            self.removeFromParent()
        }
        
        func resetValueAfterMoveToAnotherTelecom(){
            page = 0
            sims = []
            tbSim.reloadData()
        }
        
        
        @IBAction func btnAll(_ sender: Any) {
            resetValueAfterMoveToAnotherTelecom()
            updateLableColor(pos: 0)
            telecom =  all
            getSimList()
        }
        
        
        @IBAction func btnViettel(_ sender: Any) {
            resetValueAfterMoveToAnotherTelecom()
            updateLableColor(pos: 1)
            telecom =  viettel
            
            getSimList()
        }
        
        @IBAction func btnMobile(_ sender: Any) {
            resetValueAfterMoveToAnotherTelecom()
            updateLableColor(pos: 2)
            telecom =  mobile
            
            getSimList()
            
        }
        
        @IBAction func btnVina(_ sender: Any) {
            resetValueAfterMoveToAnotherTelecom()
            updateLableColor(pos: 3)
            telecom =  vina
            getSimList()
        }
        
        
        @IBAction func btnSortPrice(_ sender: Any) {
            if sort == "price.asc" {
                sort = "price.desc"
            } else {
                sort = "price.asc"
            }
            
            //            sims.removeAll()
            //            tbSim.reloadData()
            
            self.getData()
            
        }
        
        
        @IBAction func btnFilter(_ sender: Any) {
            simFilter()
        }
        
        
        @IBOutlet var btnCopySimNumber: UIButton!
        
        var copySimNumber = ""
        @IBAction func btnCopySimNumber(_ sender: Any) {
            
            
            copySimNumber = ""
            
            
            for i in 0..<selectedSims.count {
                
                if let number = selectedSims[i].alias, let price = selectedSims[i].websitePrice {
                    copySimNumber = copySimNumber + "\n" + "  " + number + " = " + String(price)
                }
                
                
            }
            
            UIPasteboard.general.string = copySimNumber
        }
        
        func updateLableColor(pos:Int){
            for i in 0..<telecoms.count {
                if i == pos {
                    telecoms[i].textColor = UIColor.init(hexString: "#5fc0ff")
                } else {
                    telecoms[i].textColor = UIColor.init(hexString: "#000000")
                }
            }
        }
        
    }
    
    extension SearhViewController : UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return sims.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: SimTableViewCell.identifier, for: indexPath) as! SimTableViewCell
            cell.selectedSim = self
            cell.tag = indexPath.row
            if( sims.count > indexPath.row ){
                cell.setPhonNumber(sims: sims, position:indexPath.row,telecom: telecom  )
                
            }
            return cell
        }
        
        func simDetail(indexPath: IndexPath, position:Int){
            isFilterPopupOpening = true
            let dialog = SimDetailPopup.instanceFromNib(simId: sims[position].id!,sim: sims[position], image: "avar", cities: cities)
            dialog.action1 = { [ weak self ] in
                let vc = CartListViewController()
                if  let simId = self?.sims[position].id {
                    vc.simId = simId
                    vc.goBackFromController = self; self?.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
            
            dialog.action2 = { [ weak self ] in
                let vc = CreateNewCartViewController()
                if  let simId = self?.sims[position].id {
                    vc.simId = simId
                    vc.goBackFromController = self; self?.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
            dialog.show()
        }
        
        func simFilter(){
            
            
            //            let noticePopup = NoticePopupView(nibName: "NoticePopupView", bundle: nil)
            //            noticePopup.view.frame = view.frame
            //            noticePopup.view.alpha = 0
            //            self.addChild(noticePopup)
            //            self.view.addSubview(noticePopup.view)
            //
            //            noticePopup.hideDialog = {
            //                //            self.isFilterPopupOpening = false
            //            }
            //
            //            UIView.animate(withDuration: 0.5, animations: {
            //                noticePopup.view.alpha = 1
            //            })
            
            
            isFilterPopupOpening = true
            let dialog = SimFilterPopup.instanceFromNib()
            dialog.action1 = { [ weak self ] in
                
            }
            
            dialog.action2  = { search in
                self.isFilterPopupOpening = false
                
                
                
                
                // Search here
                if let categoryId = search.categoryId {
                    if categoryId != 0 {
                         self.categoryId = "\(categoryId)"
                    } else {
                         self.categoryId = ""
                    }
                }
             
                self.telecom = search.telecom!
     
                
                if let tele = Int(self.telecom) {
                    self.updateLableColor(pos: tele)
                } else {
                     self.updateLableColor(pos: 0)
                }
        
                self.priceFrom = search.priceFrom!
                self.priceTo = search.priceTo!
                self.pattern = ""
                self.notContain4 = search.notAllow4!
                self.notContain7 = search.notAllow7!
                
                self.getData()
                
            }
            
            dialog.actionPressOutsideToHide = { [ weak self ] in
                self?.isFilterPopupOpening = false
            }
            
            dialog.show()
            
        }
        
        
        
        func getData(){
            sims.removeAll()
            selectedSims.removeAll()
            loadingData = true
            page = 0
            getSimList()
        }
        
        
        
        
    }
    
    extension SearhViewController : UITableViewDelegate, SelectedSim  {
        
        
        func ok(position: Int) {
            
            
            
            if !sims[position].isSelected {
                selectedSims.append(sims[position])
                
                print("SearhViewController Add \(sims[position].alias)")
                print("SearhViewController Size \(selectedSims.count)")
            } else {
                for j in 0..<selectedSims.count {
                    if sims[position].alias == selectedSims[j].alias {
                        sims[position].isSelected = !sims[position].isSelected
                        tbSim.reloadData()
                        print("SearhViewController Remove \(selectedSims[j].alias ?? "value")")
                        print("SearhViewController Size \(selectedSims.count)")
                        selectedSims.remove(at: j)
                        checkToHideCopyButton()
                        return
                        
                    }
                }
                
            }
            
            
            checkToHideCopyButton()
            sims[position].isSelected = !sims[position].isSelected
            tbSim.reloadData()
            
        }
        
        func checkToHideCopyButton(){
            if selectedSims.count > 0 {
                btnCopySimNumber.isHidden = false
                lbCopyNumberCount.text = "Copy \(selectedSims.count)"
            } else {
                btnCopySimNumber.isHidden = true
            }
            
            
        }
        
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            if sims[indexPath.row].status == 0 {
                simDetail(indexPath: indexPath,position: indexPath.row)
            }
        }
        
        
        func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
            // UITableView only moves in one direction, y axis
            let currentOffset = scrollView.contentOffset.y
            let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
            
            // Change 10.0 to adjust the distance from bottom
            if maximumOffset - currentOffset <= 10.0 {
                print("didStartAnimating")
                 getSimList()
            }
        }
     
    }
    
    extension SearhViewController: UITextFieldDelegate,GoBackFromController {
        func go(id: Int) {
            
            NotificationCenter.default.post(name: Notification.Name("needToReloadData"), object:nil)
            
            switch id {
            case ControllerId.CartListViewController.rawValue :
                getData()
                break
            case ControllerId.CreateNewCartViewController.rawValue:
                getData()
                break
            default:
                break
            }
        }
        
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            
            print(textField.text!)
            
            textField.resignFirstResponder()
            return false
        }
        
      
        
    }

    
   
