//
//  SimDetailViewController.swift
//  FastShare
//
//  Created by Tcsytems on 5/27/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit

class SimDetailViewController: UIViewController {

    @IBOutlet weak var blueView: UIView!
    @IBOutlet weak var popupView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        popupView.layer.cornerRadius = 5
        popupView.clipsToBounds = true
        
        let tapGes = UITapGestureRecognizer(target: self, action: #selector(self.closeAction))
        blueView.isUserInteractionEnabled = true
        blueView.addGestureRecognizer(tapGes)


    }
    
    @objc func closeAction() {
        self.removeFromParent()
        self.view.removeFromSuperview()
    }

}
