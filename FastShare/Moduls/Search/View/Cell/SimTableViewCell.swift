//
//  SimTableViewCell.swift
//  FastShare
//
//  Created by Tcsytems on 4/28/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit

protocol SelectedSim {
    func ok(position:Int)
}

class SimTableViewCell: UITableViewCell {

    
    @IBOutlet weak var addToCart: UIButton!
    
    @IBOutlet weak var lbPrice: UILabel!
    @IBOutlet weak var lbPhoneNumber: UILabel!
    
    @IBOutlet weak var btnCheckSimOutLet: UIButton!
    @IBOutlet var imvAva: UIImageView!
 
   
    var selectedSim:SelectedSim? = nil
    var pos = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setPhonNumber(sims:[Sim],position:Int,telecom:String){
         pos = position
        let sim = sims[position]
        
        lbPhoneNumber.text = sim.alias

        if let price = sim.websitePrice {
           lbPrice.text = "\(price.formatPriceToVND(tipAmount: price))"
        }
        
 
        
        switch sim.status {
        case 0:
            
               addToCart.isHidden = false
            break
        case 1:
           
            addToCart.isHidden = true
            break
        case 2:
        
             addToCart.isHidden = true
            break
        case 3:
          
            addToCart.isHidden = true
            break
        default:
            break
        }
        
        if sims[position].isSelected {
            if let image = UIImage(named: "Checkmark") {
                btnCheckSimOutLet.setImage(image, for: .normal)
            }
        } else {
            if let image = UIImage(named: "Checkmarkempty") {
                btnCheckSimOutLet.setImage(image, for: .normal)
            }
        }
        
//        print("SimTableViewCell LoadData \(tag)")
        
        switch sim.telco?.name {
        case "Viettel":
            imvAva.image = UIImage(named: "viettel")!
            break
        case "Vinaphone":
             imvAva.image = UIImage(named: "vina_icon")!
            break
        case "Mobifone":
            imvAva.image = UIImage(named: "mobi")!
            break
        default:
            break
        }

    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    
    @IBAction func btnBigerCheckSim(_ sender: Any) {
         selectedSim?.ok(position: pos)
    }
    @IBAction func btnCheckSim(_ sender: Any) {
        
      
       
    }
    
    
}
