//
//  PriceCollectionViewCell.swift
//  FastShare
//
//  Created by Tcsytems on 4/27/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit

class PriceCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var btnPrice: UIButton!
  
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
        
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    func setPrice(price:String){
         btnPrice.setTitle(price, for: .normal)
    }
    
    func setNiceNumber(price:SimCategory){
        btnPrice.setTitle(price.name, for: .normal)
    }

}
