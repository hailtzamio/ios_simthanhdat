//
//  PriceTableViewCell.swift
//  FastShare
//
//  Created by Tcsytems on 4/27/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit

class PriceTableViewCell: UITableViewCell {
    
   
 var simDelegate:SimDelegate? = nil
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
      let prices = ["Dưới 500 nghìn","500 - 1 triệu"," 1 -3 triệu","3 -5 triệu","5 - 10 triệu","10 -20 triệu","20 - 50 triệu","50 - 100 triệu","Trên 100 triệu"]
    
    let pricesTag = ["-500000","500000-1000000","1000000-3000000","3000000-5000000","5000000-10000000","10000000-20000000","20000000-50000000","50000000-100000000","100000000-"]
    override func awakeFromNib() {
        super.awakeFromNib()

        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.isScrollEnabled = false
    

        self.collectionView.register(UINib.init(nibName: "PriceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PriceCollectionViewCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
}

extension PriceTableViewCell: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    //
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PriceCollectionViewCell.identifier, for: indexPath) as! PriceCollectionViewCell
       
        cell.setPrice(price: prices[indexPath.row])
        
        return cell
    }
    
    
    
}

extension PriceTableViewCell: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        simDelegate?.OnItemClick(content: pricesTag[indexPath.row], type: SearchSimType.Price,title: "")
    }
}

extension PriceTableViewCell: UICollectionViewDelegateFlowLayout {
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
    //        let size = CGSize(width: (collectionView.frame.size.width - 0 )/2.2, height: (collectionView.frame.size.width - 0 )/2.0)
    //        return size
    //    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let layout = collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumLineSpacing = 15.0
        layout.minimumInteritemSpacing = 2.5
        
        let numberOfItemsPerRow: CGFloat = 3.0
        let itemWidth = (collectionView.bounds.width - layout.minimumLineSpacing) / numberOfItemsPerRow
        
        return CGSize(width: itemWidth, height: 40)
    }
}
