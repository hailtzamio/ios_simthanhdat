//
//  BornTableViewCell.swift
//  FastShare
//
//  Created by Tcsytems on 4/27/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit

class BornTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
}
