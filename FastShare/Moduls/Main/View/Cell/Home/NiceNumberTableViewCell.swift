//
//  NiceNumberTableViewCell.swift
//  FastShare
//
//  Created by Tcsytems on 4/27/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit

class NiceNumberTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
     var simDelegate:SimDelegate? = nil
//    let prices = ["Sim Vip","Sim Lộc phát","Sim Thần tài","Sim Ông địa","Sim Tam hoa","Sim Tứ quý","Sim Taxi","Sim Lặp kép","Sim Gánh đảo","Sim Tiến lên","Sim Dễ nhớ","Sim Năm sinh","Sim Số độc","Sim Ngũ quý","Sim Lục quý","Sim Tam hoa Kép","Sim Đầu cổ","Sim Tứ quý giữa","Sim Ngữ quý giữa","Sim Lục quý giữa","Sim Kép","Sim Lặp"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.isScrollEnabled = false
        self.collectionView.register(UINib.init(nibName: "PriceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PriceCollectionViewCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
}

extension NiceNumberTableViewCell: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    //
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return simCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PriceCollectionViewCell.identifier, for: indexPath) as! PriceCollectionViewCell
        cell.setNiceNumber(price: simCategories[indexPath.row])
        return cell
    }
}

extension NiceNumberTableViewCell: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let id = simCategories[indexPath.row].id {
            simDelegate?.OnItemClick(content: "\(id)", type: SearchSimType.NiceNumber, title: simCategories[indexPath.row].name!)
        }
     
    }
}

extension NiceNumberTableViewCell: UICollectionViewDelegateFlowLayout {
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
    //        let size = CGSize(width: (collectionView.frame.size.width - 0 )/2.2, height: (collectionView.frame.size.width - 0 )/2.0)
    //        return size
    //    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let layout = collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumLineSpacing = 15.0
        layout.minimumInteritemSpacing = 2.5
        
        let numberOfItemsPerRow: CGFloat = 3.0
        let itemWidth = (collectionView.bounds.width - layout.minimumLineSpacing) / numberOfItemsPerRow
        
        return CGSize(width: itemWidth, height: 40)
    }
}
