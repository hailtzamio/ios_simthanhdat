//
//  TelecomTableViewCell.swift
//  FastShare
//
//  Created by Tcsytems on 4/27/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit

protocol TelecomDelegate {
    func viettel()
    func vina()
    func mobile()
    func vietnamMobile()
    func beeLine()
    
}

class TelecomTableViewCell: UITableViewCell {

    var telecomDelegate:TelecomDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    
    @IBAction func btnVietel(_ sender: Any) {
        telecomDelegate?.viettel()
    }
    
    @IBAction func btnVina(_ sender: Any) {
        telecomDelegate?.vina()
    }
    
    @IBAction func btnMobile(_ sender: Any) {
        telecomDelegate?.mobile()
    }
    
    @IBAction func btnVietnamMobile(_ sender: Any) {
        telecomDelegate?.vietnamMobile()
    }
    
    @IBAction func btnBeeLine(_ sender: Any) {
        telecomDelegate?.beeLine()
    }
}
