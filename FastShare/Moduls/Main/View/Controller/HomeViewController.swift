//
//  HomeViewController.swift
//  FastShare
//
//  Created by Fullname on 1/17/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit

protocol SimDelegate {
    func OnItemClick(content:String, type:String
        , title:String)
}

class HomeViewController: BaseViewController, UIGestureRecognizerDelegate, SimDelegate {
    
    
    func OnItemClick(content: String, type: String, title:String) {
        pushSearhView(content: content, type: type, title: title)
    }
    
    
 
    @IBOutlet weak var tbView: UITableView!
    
    @IBOutlet weak var tfSearch: UITextField!
    let viewModel = HomeViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfSearch.addPadding(.left(15.0))
        self.navigationController?.isNavigationBarHidden = true
        tbView?.estimatedRowHeight = UITableView.automaticDimension
        //        tbView?.tableFooterView = UIView()
        
        tbView.delegate = self
        tbView.dataSource = self
        
        tbView?.register(TelecomTableViewCell.nib, forCellReuseIdentifier: TelecomTableViewCell.identifier)
        tbView?.register(BornTableViewCell.nib, forCellReuseIdentifier: BornTableViewCell.identifier)
        tbView?.register(PriceTableViewCell.nib, forCellReuseIdentifier: PriceTableViewCell.identifier)
        tbView?.register(NiceNumberTableViewCell.nib, forCellReuseIdentifier: NiceNumberTableViewCell.identifier)
        
          swipeToBack()
        
//        viewModel.getDataSucessfully = { [weak self]  in
//            if let data = self?.viewModel.data {
//                self?.simCategories = data
//                self?.tbView.reloadData()
//            }
//        }
        
        viewModel.getDataFail = {
            print("updateLoadingStatus")
        }
        
        viewModel.getSimCaterogy()
        
        
    }
    
    @IBAction func btnSearch(_ sender: Any) {
        pushSearhView(content: "0", type: SearchSimType.Telecom,title: "Tìm Kiếm Sim" )
    }
    
    func addSearhView(content:String,type:String, title:String){
        let controller = getControllerFromHome(id: .Search) as! SearhViewController
        controller.content = content
        controller.simSearchType = type
        controller.searchTitle = title
        controller.view.frame = self.view.bounds;
        controller.willMove(toParent: self)
        self.view.addSubview(controller.view)
        self.addChild(controller)
        controller.didMove(toParent: self)
    }
    
    
    func pushSearhView(content:String,type:String, title:String){
        let controller = getControllerFromHome(id: .Search) as! SearhViewController
        controller.content = content
        controller.simSearchType = type
        controller.searchTitle = title
        controller.view.frame = self.view.bounds;
       navigationController?.pushViewController(controller, animated: false)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        swipeToPop()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func swipeToBack() {
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true;
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self as! UIGestureRecognizerDelegate;
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if gestureRecognizer == self.navigationController?.interactivePopGestureRecognizer {
            return false
        }
        return true
    }
    
}

extension HomeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch indexPath.row {
        case 0 :
            if let cell = tableView.dequeueReusableCell(withIdentifier: TelecomTableViewCell.identifier, for: indexPath) as? TelecomTableViewCell {
                cell.telecomDelegate = self
                return cell
            }
            //        case 1 :
            //            if let cell = tableView.dequeueReusableCell(withIdentifier: BornTableViewCell.identifier, for: indexPath) as? BornTableViewCell {
            //                return cell
            //            }
            
        case 1 :
            if let cell = tableView.dequeueReusableCell(withIdentifier: PriceTableViewCell.identifier, for: indexPath) as? PriceTableViewCell {
                cell.simDelegate = self
                return cell
            }
            
        case 2 :
            if let cell = tableView.dequeueReusableCell(withIdentifier: NiceNumberTableViewCell.identifier, for: indexPath) as? NiceNumberTableViewCell {
                cell.simDelegate = self
                return cell
            }
            
        default:
            return UITableViewCell()
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt
        indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 1{
            
            return 3*50 + 55
        } else  if indexPath.row == 2{
            let totalRow = (simCategories.count/3 + 1)
            return CGFloat(totalRow*50 + 100)
        } else {
            return UITableView.automaticDimension
        }
    }
    
}

extension HomeViewController: UITableViewDelegate {
    
}

extension HomeViewController: TelecomDelegate {
    func viettel() {
        pushSearhView(content: "1", type: SearchSimType.Telecom, title :"Viettel")
    }
    
    func vina() {
        pushSearhView(content: "3", type: SearchSimType.Telecom, title: "Vina")
    }
    
    func mobile() {
        pushSearhView(content: "2", type: SearchSimType.Telecom, title:"Mobile")
    }
    
    func vietnamMobile() {
//        pushSearhView(content: "Vietnam Mobile", type: SearchSimType.Telecom, title:"" )
    }
    
    func beeLine() {
        
    }
    
}

