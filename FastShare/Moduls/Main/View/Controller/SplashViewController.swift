//
//  SplashViewController.swift
//  FastShare
//
//  Created by Fullname on 2/27/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit

class SplashViewController: BaseViewController {
    let viewModel = LoginViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.checkInternet = { [weak self] in
            self?.toastShort(content: error_check_internet)
        }
        
        if preferences.object(forKey: access_token_key) == nil {
            let vc = getControllerFromHome(id: .Login)
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
             getNewToken()
        }

    }
    
    // run Api login to get new Token
    
    func getNewToken(){
        
      
        
        viewModel.getDataSucessfully = { [weak self]  in
            if (self?.viewModel.data) != nil && self?.viewModel.simCategory == nil {
                 self?.preferences.set(self?.viewModel.data?.access_token, forKey: access_token_key)
                Production.ACCESS_TOKEN = (self?.viewModel.data?.access_token)!
                self?.viewModel.getSimCaterogy()
            }
            
            if(self?.viewModel.simCategory != nil ) {
                simCategories = (self?.viewModel.simCategory)!
                let vc = getControllerFromHome(id: .Main)
                self?.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        viewModel.getDataFail = { [weak self] in
           self?.toastShort(content: "Can not get Token!")
        }
        
        viewModel.login2(with: preferences.object(forKey: number_phone_key) as! String, password: preferences.object(forKey: passwork_key) as! String)
        
       
        
    }
    



}
