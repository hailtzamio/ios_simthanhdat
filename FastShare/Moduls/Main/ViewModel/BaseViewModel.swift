//
//  BaseViewModel.swift
//  FastShare
//
//  Created by Fullname on 1/18/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit

enum TableViewItemType {
    case one
    case two
    case three
    case four
}

protocol TableViewModelItem {
    var type: TableViewItemType { get }
    var sectionTitle: String { get }
    var rowCount: Int { get }
}

class BaseViewModel {

    var showAlertClosure: (() -> ())?
    var getDataFail: (() -> ())?
    var getDataSucessfully: (() -> ())?
    var checkInternet: (() -> ())?

    var error: Error? {
        didSet { self.showAlertClosure?() }
    }
    
    var isLoading: Bool = false {
        didSet { self.getDataFail?() }
    }

}
