//
//  HomeViewModel.swift
//  FastShare
//
//  Created by Fullname on 1/18/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit

class HomeViewModel : BaseViewModel {
    
    
    // MARK: - Properties
    
    var data: [SimCategory]? {
        didSet {
            guard data != nil else { return }
            self.getDataSucessfully?()
        }
    }
    
    func getSimCaterogy(){
        
        MGConnection.requestDataList(APIRouter.getSimCaterogy(), SimCategory.self) { (result, error) in
            
            if(error?.mErrorCode == 40){
                self.checkInternet?()
                return
            }
            
            guard error == nil else {
                self.isLoading = false
                return
            }
            
            if let listSim = result {
                self.data = listSim
                SimCategory.add(data: listSim)
                return
            }
        }
        
    }
}



