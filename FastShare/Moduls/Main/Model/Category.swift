//
//  Category.swift
//  FastShare
//
//  Created by Tcsytems on 5/31/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import ObjectMapper

class Category:  NSObject, Mappable, NSCoding  {
    
    var id: Int?
    var code: String?
    var name: String?
    var priority:Int?
    
    override init() {}
    
    
   required convenience init?(map: Map) {
        self.init()
        mapping(map: map)
    
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        code <- map["code"]
        name <- map["name"]
        priority <- map["priority"]
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(id, forKey: "id")
        coder.encode(code, forKey: "code")
        coder.encode(name, forKey: "name")
        coder.encode(priority, forKey: "priority")
    }
    
    required init(coder decoder: NSCoder) {
        self.id = decoder.decodeObject(forKey: "id") as? Int ?? 1
        self.code = decoder.decodeObject(forKey: "code") as? String ?? ""
        self.name = decoder.decodeObject(forKey: "name") as? String ?? ""
        self.priority = decoder.decodeObject(forKey: "priority") as? String ?? ""
    }
 

}
