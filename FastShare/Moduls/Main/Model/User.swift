//
//  User.swift
//  FastShare
//
//  Created by Fullname on 1/18/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import ObjectMapper

class User: NSObject, Mappable, NSCoding {

    

    var access_token: String?
    var full_name: String?
    var email: String?
    var phone:String?
    var roles:String?

     override init() {}
    
    required convenience init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map)
    {
        access_token <- map["access_token"]
        full_name <- map["full_name"]
        email <- map["email"]
        phone <- map["phone"]
          roles <- map["roles"]
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(access_token, forKey: "access_token")
        coder.encode(full_name, forKey: "full_name")
        coder.encode(email, forKey: "email")
        coder.encode(phone, forKey: "phone")
        coder.encode(roles, forKey: "roles")
    }
    
    required init(coder decoder: NSCoder) {
        self.access_token = decoder.decodeObject(forKey: "access_token") as? String ?? ""
        self.full_name = decoder.decodeObject(forKey: "full_name") as? String ?? ""
        self.email = decoder.decodeObject(forKey: "email") as? String ?? ""
        self.phone = decoder.decodeObject(forKey: "phone") as? String ?? ""
           self.roles = decoder.decodeObject(forKey: "roles") as? String ?? ""
    }

}


class Roles:Mappable {
    
    var roles:[String]?
    
    required convenience init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map)
    {
        roles <- map["roles"]
    }
}
