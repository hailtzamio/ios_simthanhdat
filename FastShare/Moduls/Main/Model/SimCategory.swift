//
//  Category.swift
//  FastShare
//
//  Created by Tcsytems on 5/31/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

@objcMembers class SimCategory: Object,Mappable  {
    
    var id: Int?
    var code: String?
    var name: String?
    var priority:Int?
    
    
   required convenience init?(map: Map) {
        self.init()
        mapping(map: map)
    
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        code <- map["code"]
        name <- map["name"]
        priority <- map["priority"]
    }
    

}

extension SimCategory {
    static func add(data:[SimCategory] , in realm: Realm = try! Realm()) {
        for item in data {
            try! realm.write {
                realm.add(item)
            }
        }
    }
    
    static func getSimCategory(in realm: Realm = try! Realm()) -> Results<SimCategory> {
        return realm.objects(SimCategory.self)
    }
}
