//
//  CreateNewCartViewController.swift
//  FastShare
//
//  Created by Ambi on 7/4/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit

class CreateNewCartViewController: BaseViewController {

    let viewModel = InvoiceViewModel()
    @IBOutlet weak var tfName: UITextField!
    
    @IBOutlet weak var tfPhone: UITextField!
    
    @IBOutlet weak var tfAddress: UITextField!
    var simId = 0
     var goBackFromController:GoBackFromController? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        
     


}
    
    
    @IBAction func btnOk(_ sender: Any) {
        
        if (tfName.text!.trimmingCharacters(in: .whitespaces) == "" || tfPhone.text!.trimmingCharacters(in: .whitespaces) == "" || tfAddress.text!.trimmingCharacters(in: .whitespaces) == "") {
            toastShort(content: "Nhập chưa đủ thông tin.")
            return
        }
    
        viewModel.addNewCart(simId: simId, customerName: tfName.text!, customerPhone: tfPhone.text!, customerAddress: tfAddress.text!, completion : {(response) in
            print("Okieeeee")
            if self.goBackFromController != nil {
                self.goBackFromController?.go(id: ControllerId.CreateNewCartViewController.rawValue)
                self.navigationController?.popViewController(animated: true)
            }
        }){ (error) in
            
        }
    }
    
    
    @IBAction func btnCancel(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
