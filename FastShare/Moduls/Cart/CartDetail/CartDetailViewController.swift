//
//  CartDetailViewController.swift
//  FastShare
//
//  Created by Tcsytems on 7/5/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit

class CartDetailViewController: BaseViewController {
    
    
    @IBOutlet weak var lbCustomerName: UILabel!
    @IBOutlet weak var lbStatus: UILabel!
    var viewModel = InvoiceViewModel()
    var cartId = 0
    var sims = [Invoice]()
    var myCart:MyCart? = nil
    
    @IBOutlet weak var btnRequestAndPay: UIButton!
    @IBOutlet weak var tbView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
         swipeToPop()
        
        tbView.dataSource = self
        tbView.delegate = self
        
        tbView?.register(CartTableViewCell.nib, forCellReuseIdentifier: CartTableViewCell.identifier)
        
        
        viewModel.getDataSucessfully = {[ weak self ] in
            
            if let myCart = self?.viewModel.myCart {
                self?.myCart = myCart
                self?.sims = myCart.reservations!
              
                if myCart.orderStatus == "new" {
                    self?.lbStatus.text = "Mới đặt, cần gửi yêu cầu cho đại lý."
                } else if myCart.orderStatus == "placed" {
                    self?.lbStatus.text = "Chờ đại lý duyệt."
                } else if myCart.orderStatus == "readyToPay" {
                     self?.lbStatus.text = "Sẵn sàng thanh toán."
                } else if myCart.orderStatus == "cancelled" {
                    self?.btnRequestAndPay.isHidden = true
                    self?.lbStatus.text = "Đại lý đã huỷ đơn."
                   
                } else if myCart.orderStatus == "done" {
                    self?.lbStatus.text = "Hoàn thành."
                     self?.btnRequestAndPay.isHidden = true
                }
                
                
                self?.lbCustomerName.text = myCart.customer?.name!
                self?.tbView.reloadData()
            }
            
        }
        
        viewModel.getCartById(cartId: cartId)
    }
    
    
    @IBAction func btnSendRequestToAgency(_ sender: Any) {
        
        if let cart = myCart {
            if cart.orderStatus == "new" {
                let payCart = PayCart()
                payCart.cartId = cart.id!
                sendRequestToAgencyWaitForApprove(payCart: payCart)
            } else {
                let payCart = PayCart()
                payCart.cartId = cart.id!
                payCart.customerName = cart.customer!.name!
                payCart.customerPhone = cart.customer!.phone!
                payCart.customerAddress = cart.customer!.address!
                payAfterServerApprove(payCart:payCart)
            }
        }
    }
    
    func sendRequestToAgencyWaitForApprove(payCart:PayCart){
        showLoading()
        viewModel.sendRequestToAgency(payCart: payCart, completion : {(response) in
            self.stopLoading()
//            self.navigationController?.popViewController(animated: true)
            self.viewModel.getCartById(cartId: self.cartId)
            self.toastShort(content: "Đã gửi yêu cầu cho đại lý.")
        }){ (error) in
            self.stopLoading()
        }
        
    }
    
    func payAfterServerApprove(payCart:PayCart){
        showLoading()
        viewModel.payCart(payCart: payCart, completion : {(response) in
            self.stopLoading()
//            self.navigationController?.popViewController(animated: true)
            self.viewModel.getCartById(cartId: self.cartId)
            self.toastShort(content: "Thanh toán thành công.")
        }){ (error) in
            self.stopLoading()
        }
    }
    
    
    @IBAction func btnBack(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("needToReloadData"), object:nil)
        navigationController?.popViewController(animated: true)
    }
    
 
}

extension CartDetailViewController:UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return sims.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CartTableViewCell.identifier, for: indexPath) as! CartTableViewCell
        cell.fetchData(invoice: sims[indexPath.row])
        
        return cell
    }
    
    
}
