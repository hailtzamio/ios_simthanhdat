//
//  CartListViewController.swift
//  FastShare
//
//  Created by Ambi on 7/4/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit

protocol GoBackFromController {
    func go(id:Int)
}

class CartListViewController: BaseViewController {

    @IBOutlet weak var tbView: UITableView!
    var data = 20
    private var currentPage = 0
    var refreshContro = UIRefreshControl()
    var isLoading = false
    var viewModel = InvoiceViewModel()
    var simId = 0
    var cartId = 0
    var myCarts = [MyCart]()
    var goBackFromController:GoBackFromController? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tbView.dataSource = self
        tbView.delegate = self
        
        tbView.separatorStyle = .none
        tbView.register(MyCartTableViewCell.nib, forCellReuseIdentifier: MyCartTableViewCell.identifier)
        
        
        // Refresh control add in tableview.
        refreshContro.attributedTitle = NSAttributedString(string: "Làm mới")
        refreshContro.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.tbView.addSubview(refreshContro)
          showLoading()
        viewModel.getDataSucessfully = { [weak self ] in
            self?.refreshContro.endRefreshing()
            self?.stopLoading()
            if let myCarts = self?.viewModel.myCarts {
                for i in 0..<myCarts.count {
                    if myCarts[i].status == "processing" {
                        self?.myCarts.append(myCarts[i])
                    }
                }
                self?.tbView.reloadData()
            }
            
        }
        
        viewModel.getMyCart(page: currentPage)
        
        
        
    }
    
    @objc func refresh(_ sender: Any) {
        self.myCarts.removeAll()
        self.tbView.reloadData()
        viewModel.getMyCart(page: currentPage)
    }
    
    private func fetchNextPage() {
//        currentPage += 1
//        getData()
    }
    
    
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    func addSimToCart(){
        showLoading()
        viewModel.holdTheSim(simId: simId,cartId:cartId, completion : {(response) in
           self.toastShort(content: "Đã thêm vào giỏ")
            self.stopLoading(); self.navigationController?.popViewController(animated: true)
            
            if self.goBackFromController != nil {
                self.goBackFromController?.go(id: ControllerId.CartListViewController.rawValue)
            }
            
        }){ (error) in
            self.stopLoading()
            self.toastShort(content: "Có lỗi ...")
        }
        
    }
}

extension CartListViewController:UITableViewDataSource, UITableViewDelegate {
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MyCartTableViewCell.identifier, for: indexPath) as! MyCartTableViewCell
        cell.fetchData(myCart: myCarts[indexPath.row])
        return cell
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myCarts.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        cartId = myCarts[indexPath.row].id!
        
        let alert = UIAlertController(title: "Bạn có muốn thêm vào giỏ hàng?", message: "", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Đồng ý", style: .default, handler: { action in
            self.addSimToCart()
        }))
        alert.addAction(UIAlertAction(title: "Huỷ", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
}
