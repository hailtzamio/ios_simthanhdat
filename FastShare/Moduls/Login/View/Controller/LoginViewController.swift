//
//  LoginViewController.swift
//  FastShare
//
//  Created by Fullname on 1/17/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import ViewAnimator
import BLTNBoard
class LoginViewController: BaseViewController {
    
    
    @IBOutlet weak var lbSimThanhDat: UILabel!
    @IBOutlet weak var tfPassword: UITextField!
    
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var btnForgetPassword: UIButton!
    
    let viewModel = LoginViewModel()
    var dict : [String : AnyObject]!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnForgetPassword.underline()
        tfUsername.addPadding(.left(50.0))
        tfPassword.addPadding(.left(50.0))
        lbSimThanhDat.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            
        }
        
        tfUsername.text = "0222222222"
       tfUsername.text =  "0393888888"
        tfPassword.text = "123456"
        tfPassword.isSecureTextEntry = true
        viewModel.getDataSucessfully = { [weak self]  in
            if (self?.viewModel.data) != nil && self?.viewModel.simCategory == nil {
                self?.stopLoading()
                self?.preferences.set(self?.viewModel.data?.access_token, forKey: access_token_key)
                self?.preferences.set(self?.tfUsername.text, forKey: number_phone_key)
                self?.preferences.set(self?.tfPassword.text, forKey: passwork_key)
//                self?.preferences.set(self?.viewModel.data?.roles![0], forKey: user_role_key)
//                
//                print("Roles === \(self?.viewModel.data?.roles![0])")
                
                 Production.ACCESS_TOKEN = (self?.viewModel.data?.access_token)!
//                 Production.USER_ROLE =
                 self?.viewModel.getSimCaterogy()
                
            }
            
            if(self?.viewModel.simCategory != nil ) {
                simCategories = (self?.viewModel.simCategory)!
                let vc = getControllerFromHome(id: .Main)
                self?.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        viewModel.getDataFail = { [weak self] in
            self?.stopLoading()
            self?.toastShort(content: "Wrong Username Or Password!")
            print("updateLoadingStatus")
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        lbSimThanhDat.isHidden = false
//        let animation = AnimationType.zoom(scale: 0.5)
//        self.lbSimThanhDat.animate(animations: [animation])
    }
    
    @IBAction func btnLogin(_ sender: Any) {
        
        viewModel.checkInternet = { [weak self] in
            self?.toastShort(content: error_check_internet)
        }
        
        if tfPassword.text == "" || tfUsername.text == "" {
            toastShort(content: "Nhập số điện thoại và mật khẩu.")
            return
        }
        
        showLoading()
        viewModel.login2(with: tfUsername.text!, password: tfPassword.text!)
        
    }
    
    @IBAction func btnRetrievPassword(_ sender: Any) {
        
        
    }
    
    @IBAction func btnRegister(_ sender: Any) {
        let vc = getControllerFromHome(id: .Register)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
