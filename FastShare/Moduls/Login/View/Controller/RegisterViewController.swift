//
//  RegisterViewController.swift
//  FastShare
//
//  Created by Tcsytems on 4/27/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit

class RegisterViewController: BaseViewController {
    
    
    @IBOutlet weak var tfPhoneNumber: RadiusTextField!
    
    @IBOutlet weak var tfPassword: RadiusTextField!
    
    @IBOutlet weak var tfName: RadiusTextField!
    
    @IBOutlet weak var tfCity: RadiusTextField!
    @IBOutlet weak var tfConfirmPasword: RadiusTextField!
    let viewModel = LoginViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnRegister(_ sender: Any) {
        
        if ( tfName.text == "" || tfPhoneNumber.text == "" || tfPassword.text == "" || tfConfirmPasword.text == "" || tfCity.text == "" ) {
            
            toastShort(content: "Input")
            return
        }
        
        if tfPassword.text != tfConfirmPasword.text {
            
            toastShort(content: "Wrong Password!")
            return
        }
        
        viewModel.registerAc(with: tfName.text!, password: tfPassword.text!, email: tfCity.text!, phone: tfPhoneNumber.text!)
        
        viewModel.getDataSucessfully = { [weak self]  in
            if let data = self?.viewModel.register {
                self?.stopLoading()
               
            }
        }
        
        viewModel.getDataFail = {
            
        }
    }
}
