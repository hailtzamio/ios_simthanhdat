//
//  Register.swift
//  FastShare
//
//  Created by Tcsytems on 4/30/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import ObjectMapper
class Register: Mappable {
    
    var access_token: String?
    var full_name: String?
    var email: String?
    var phone:String?
    var message:String?
    var status:Int?
    
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map)
    {
          status <- map["status"]
        message <- map["message"]
        access_token <- map["access_token"]
        full_name <- map["full_name"]
        email <- map["email"]
        phone <- map["phone"]
    }
    
}
