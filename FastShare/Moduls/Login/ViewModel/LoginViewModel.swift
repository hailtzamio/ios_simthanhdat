//
//  LoginViewModel.swift
//  FastShare
//
//  Created by Tcsytems on 4/28/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import Alamofire
class LoginViewModel : BaseViewModel  {
    
    var data:User? {
        didSet {
            guard data != nil else { return }
            self.getDataSucessfully?()
        }
    }
    
    var register:Register? {
        didSet {
            guard data != nil else { return }
            self.getDataSucessfully?()
        }
    }
    
    var simCategory: [SimCategory]? {
        didSet {
            guard data != nil else { return }
            self.getDataSucessfully?()
        }
    }
    
    func register2(with fullname:String, password:String, email:String, phone:String) {
        
        MGConnection.request(APIRouter.register(fullname: fullname, password: password,email: email, phone:phone), Register.self,
                             completion: {(result, error) in
                                guard error == nil else {
                                    self.isLoading = false
                                    return
                                }
                                
                                if let register = result {
                                    self.register = register
                                    return
                                }
        })
    }
    
    func login2(with username: String, password:String){
        
        if !NetworkReachabilityManager()!.isReachable {
            self.checkInternet?()
            return
        }
        
        let url = "http://27.118.22.28:9125/oauth/token"
        var _headers : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
        _headers["Authorization"] = "Basic YXBwMToxMjM0NTY="
        let params : Parameters = ["grant_type":"password","username":username,"password":password]
        
        request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody , headers: _headers).responseJSON(completionHandler: {
            response in response
            
            switch response.result {
            case .success:
                let jsonResponse = response.result.value as! NSDictionary
                if jsonResponse["access_token"] != nil {
                    let user = User()
                    user.access_token = jsonResponse["access_token"] as? String
                    user.full_name = jsonResponse["full_name"] as? String
                    user.phone = jsonResponse["phone"] as? String
                    user.email = jsonResponse["email"] as? String
                    user.roles = (jsonResponse["roles"] as! [String])[0]
                    Production.USER_ROLE = (jsonResponse["roles"] as! [String])[0]
                    Context.saveUserLogin(userLogin: user)
                    self.data = user
                } else {
                    self.isLoading = false
                }
                
                break
                
            case .failure(let error):
                if error is AFError {
                    self.isLoading = false
                }
                
                break
            }
            
            
        })
    }
    
    func registerAc(with fullname:String, password:String, email:String, phone:String){
        let url = "http://simthanhdat.zamio.net:9125/api/register"
        var _headers : HTTPHeaders = ["Content-Type": "application/json"]
        _headers["Authorization"] = "Basic YXBwMToxMjM0NTY="
        let params : Parameters = ["fullname":fullname,"password":password,"email":email,"phone":phone]
        
        request(url, method: .post, parameters: params, encoding: URLEncoding.default , headers: _headers).responseData(completionHandler: {
            response in response
            
            switch response.result {
            case .success:
                let jsonResponse = response.result.value as! NSDictionary
                if jsonResponse["access_token"] != nil {
                    let user = User()
                    user.access_token = jsonResponse["access_token"] as? String
                    user.full_name = jsonResponse["full_name"] as? String
                    user.phone = jsonResponse["phone"] as? String
                    user.email = jsonResponse["email"] as? String
                    self.data = user
                }
                break
                
            case .failure(let error):
                if error is AFError {
                    self.isLoading = false
                }
                
                break
            }
            
            
        })
    }
    
    
    func getSimCaterogy(){
        
        MGConnection.requestDataList(APIRouter.getSimCaterogy(), SimCategory.self) { (result, error) in
            
            if(error?.mErrorCode == 40){
                self.checkInternet?()
                return
            }
            
            guard error == nil else {
                self.isLoading = false
                return
            }
            
            if let listSim = result {
                self.simCategory = listSim
                SimCategory.add(data: listSim)
                return
            }
        }
        
    }
    
}
