//
//  UpdateProfileViewController.swift
//  FastShare
//
//  Created by Tcsytems on 4/29/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import TOCropViewController

protocol UpdateProfileProtocol {
    func reloadData()
}
class UpdateProfileViewController: BaseViewController, UINavigationControllerDelegate {
    
    @IBOutlet weak var tfName: RadiusTextField!
     @IBOutlet weak var tfEmail: RadiusTextField!
    @IBOutlet weak var tfPhone: RadiusTextField!
    @IBOutlet weak var imvAva: UIImageView!
    @IBOutlet weak var btnAvaOutlet: UIButton!
    var delegate: UpdateProfileProtocol? = nil
    let viewModel = ProfileViewModel()
    var uiImage:UIImage? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let user = Context.getUserLogin()

        if let phone = Context.getUserLogin()?.phone {
            tfPhone.text = "\(phone)"
            tfPhone.isEnabled = false
        }
        
        tfName.text = user?.full_name
        tfEmail.text = user?.email
    }
    
    func updateProfile(){
        
        let profileParam = [
            "fullName": tfName.text as! String,
            "email": tfEmail.text as! String
            ] as [String : Any]
        
        if let data = uiImage!.pngData() {
            showLoading()
            viewModel.updateProfile(imageData: data, parameters: profileParam)
                
            viewModel.getDataSucessfully = { [weak self ] in
                self?.stopLoading()
                self?.toastShort(content: "Cập nhật thành công.")
                self?.delegate?.reloadData()
            }
            
            viewModel.getDataFail = { [weak self ] in
                self?.stopLoading()
                self?.toastShort(content: "Không thành công.")
            }
        }
       
        
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnChangeAva(_ sender: Any) {
          self.pickImage(isLibrary: true)
    }
    
    @IBAction func btnUpdateProfile(_ sender: Any) {
        updateProfile()
    }
    
}

extension UpdateProfileViewController : UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        let cropVC = TOCropViewController.init(croppingStyle: .default, image: image )
        cropVC.delegate = self
        cropVC.aspectRatioPreset = .presetSquare
        cropVC.aspectRatioLockEnabled = true
        cropVC.resetAspectRatioEnabled = false
        cropVC.aspectRatioPickerButtonHidden = true
        picker.dismiss(animated: false) {
            self.present(cropVC, animated: false, completion: nil)
        }
    }
}

extension UpdateProfileViewController : TOCropViewControllerDelegate {
    func pickImage(isLibrary: Bool) {
        
        let picker = UIImagePickerController()
        picker.sourceType = isLibrary ?  .photoLibrary : .camera
        picker.allowsEditing = false;
        picker.delegate = self;
        self.present(picker, animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didCropTo image: UIImage, with cropRect: CGRect, angle: Int) {
        
        uiImage = image
        imvAva.image = image
         imvAva.layer.cornerRadius = 40
        
        cropViewController.dismiss(animated: true, completion: nil)
        
        
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
        cropViewController.dismiss(animated: true, completion: {
            
        })
    }
}
