//
//  ProfileViewController.swift
//  FastShare
//
//  Created by Tcsytems on 4/29/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import Kingfisher



class ProfileViewController: BaseViewController, UpdateProfileProtocol {
  
    
  var loginNav:UINavigationController? = nil
      @IBOutlet weak var tfName: UILabel!
     @IBOutlet weak var tfPhone: UILabel!
     @IBOutlet weak var tfEmail: UILabel!
     @IBOutlet weak var tfAddress: UILabel!
    
    
    @IBOutlet var imvAvatar: UIImageView!
 
    var viewModel = ProfileViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        swipeToPop()
        
      self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
        
        viewModel.getDataSucessfully = {[weak self ] in
            if let profile = self?.viewModel.profile {
                self?.tfName.text = profile.fullName
                self?.tfPhone.text = profile.phone
                self?.tfEmail.text = profile.email
                self?.tfAddress.text  = profile.province
               
                let url = profile.avatar!
                self?.imvAvatar.kf.setImage(with: URL(string: url))
            }
        }
        
        viewModel.getMyProfile()
        
      
    }
    
    func reloadData() {
        viewModel.getMyProfile()
    }
    
    @IBAction func btnLogout(_ sender: Any) {
      preferences.set(nil
        , forKey: access_token_key)
        Production.ACCESS_TOKEN = ""
        Switcher.updateRootVC()
    }
    
    @IBAction func btnUpdateProfile(_ sender: Any) {
            let controller = getControllerFromProfile(id: .UpdateProfile) as! UpdateProfileViewController
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
//        self.navigationController?.present(controller, animated: true, completion: nil)
        
    }
    
    @IBAction func changePassword(_ sender: Any) {
        let controller = getControllerFromProfile(id: .ChangePassword)
        self.navigationController?.pushViewController(controller, animated: true)
        
        let user = Context.getUserLogin()
        print(user?.access_token)
    }
}
