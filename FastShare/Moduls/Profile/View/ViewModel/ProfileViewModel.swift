//
//  ProfileViewModel.swift
//  FastShare
//
//  Created by Tcsytems on 5/27/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
class ProfileViewModel: BaseViewModel {
    
    var data:[Sim]? {
        didSet {
            guard data != nil else { return }
            self.getDataSucessfully?()
        }
    }
    
    var profile:MyProfile? {
        didSet {
            guard profile != nil else { return }
            self.getDataSucessfully?()
        }
    }
    
    
    // For ProfileViewController
    func getMyProfile(){
        
        MGConnection.request(APIRouter.myProfile(), MyProfile.self) { (result, error) in
            guard error == nil else {
                self.isLoading = false
                return
            }
            
            if let profile = result {
                self.profile = profile
                return
            }
        }
        
    }
    
    func updateProfile(imageData: Data?, parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        let url = Production.BASE_URL + "update-profile"
        
        var headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]   
        headers["Authorization"] = "Bearer " + Production.ACCESS_TOKEN
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            let randomIntFrom0To10 = Int.random(in: 1..<1000)
            
            if let data = imageData{
                multipartFormData.append(data, withName: "ava"+String(randomIntFrom0To10), fileName: "image.png", mimeType: "image/jpeg")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    if response.response?.statusCode == 200 {
                        self.getDataSucessfully?()
                    } else {
                        self.getDataFail?()
                    }
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                
            }
        }
    }
}
