//
//  User.swift
//  FastShare
//
//  Created by Tcsytems on 6/27/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import ObjectMapper

class MyProfile:Mappable  {
    
    var id:Int?
    var username: String?
    var phone: String?
    var email:String?
    var fullName:String?
    var province:String?
    var district:String?
    var ward:String?
    var cmnd1:String?
     var cmnd2:String?
    var avatar:String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map)
    {
        id <- map["id"]
        username <- map["username"]
        phone <- map["phone"]
        email <- map["email"]
        fullName <- map["fullName"]
        province <- map["province"]
        district <- map["district"]
        ward <- map["ward"]
        cmnd1 <- map["cmnd1"]
        cmnd2 <- map["cmnd2"]
        avatar <- map["avatar"]
    }
    
}
