//
//  CartTableViewCell.swift
//  FastShare
//
//  Created by Ambi on 7/7/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit

class CartTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tfNumberPhone: UILabel!
    
    @IBOutlet weak var tfTime: UILabel!
    @IBOutlet weak var tfPrice: UILabel!
    
    @IBOutlet weak var lbStatus: UILabel!
    
    
    
    @IBOutlet weak var lbSimStatus: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    func fetchData(invoice:Invoice){
        tfTime.text = invoice.createdAt
        tfNumberPhone.text = invoice.number
        lbSimStatus.text = invoice.cateName
        
        
        
        lbStatus.text = invoice.agencyResponse!
        if let price = invoice.websitePrice{
            tfPrice.text =  price.formatPriceToVND(tipAmount: price)
        }
    }
    
    func fetchData(sim:Sim){
        tfTime.text = sim.createdAt
        tfNumberPhone.text = sim.number
        lbSimStatus.text = sim.cateName
        lbStatus.text = sim.agencyResponse
        if let price = sim.websitePrice{
            tfPrice.text =  price.formatPriceToVND(tipAmount: price)
        }
        
        if sim.agencyResponse == "ok" {
            lbStatus.text = "Đã bán"
            lbStatus.textColor = UIColor.init(hexString: "#5FC0FF")
        } else if sim.agencyResponse == "simNotAvailable" {
            lbStatus.text = "Đã huỷ"
            lbStatus.textColor = UIColor.init(hexString: "#FF0E00")
        } else {
            lbStatus.text = "Đang giao dịch"
            
        }
    }
    
    func updateSimStatus(status:SimStatus){
        
        switch status {
        case .new:
            lbSimStatus.textColor = .blue
            lbSimStatus.text = "Mới đặt"
            break
        case .processing:
            lbSimStatus.textColor = .green
            lbSimStatus.text = "Giao dịch"
            break
        case .done:
            lbSimStatus.textColor = .red
            lbSimStatus.text = "Hoàn tất"
            break
        default:
            break
        }
        
    }
    
}
