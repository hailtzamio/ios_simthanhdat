//
//  MyCartTableViewCell.swift
//  FastShare
//
//  Created by Ambi on 6/30/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit

class MyCartTableViewCell: UITableViewCell {

    

    @IBOutlet weak var lbStatus: UILabel!
    @IBOutlet var totalPrice: UILabel!
    
    @IBOutlet var lbDate: UILabel!
    
    @IBOutlet var lbCustomer: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    func fetchData(myCart:MyCart){
      
        
        if let totalAmount = myCart.totalAmount {
            totalPrice.text = totalAmount.formatPriceToVND(tipAmount: totalAmount)
        }
        if myCart.customerName != nil {
            lbCustomer.text = myCart.customerName!
        }
        lbDate.text = myCart.createdDate
        
        if myCart.status == "processing" {
            lbStatus.text = "Đang giao dịch"
               lbStatus.textColor = UIColor.init(hexString: "#FF9300")
        } else if myCart.status == "done" {
            lbStatus.text = "Hoàn tất"
            lbStatus.textColor = UIColor.init(hexString: "#5FC0FF")
        } else {
            lbStatus.text = "Đã huỷ"
            lbStatus.textColor = UIColor.init(hexString: "#FF0E00")
        }
        
//        if let price = myCart.affiliatePrice{
//            totalPrice.text =  "\(price)"
//        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
