//
//  Invoice.swift
//  FastShare
//
//  Created by Tcsytems on 6/20/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import ObjectMapper

class Invoice:Mappable  {
    
    var id:Int?
    var simNumber: String?
    var simAlias: String?
    var cateName:String?
    var status:Int?
    var createdAt:String?
    var statusText:String?
    var affiliatePrice:CLong?
    var websitePrice:CLong?
    var number:String?
    var agencyResponse:String?
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map)
    {
        id <- map["id"]
        number <- map["number"]
        simNumber <- map["simNumber"]
        simAlias <- map["simAlias"]
        cateName <- map["cateName"]
        status <- map["status"]
        createdAt <- map["createdAt"]
        statusText <- map["statusText"]
        affiliatePrice <- map["affiliatePrice"]
        websitePrice <- map["websitePrice"]
         agencyResponse <- map["agencyResponse"]
    }
    
}
