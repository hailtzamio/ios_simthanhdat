//
//  PayCart.swift
//  FastShare
//
//  Created by Ambi on 6/30/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import ObjectMapper
class PayCart: Mappable {
 
    var cartId: Int?
    var customerName: String = ""
    var customerPhone: String = ""
    var customerAddress: String = ""
    var provinceId:Int = 1
    var districtId:Int = 1
    var wardId:Int = 1
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map)
    {
        cartId <- map["cartId"]
        customerName <- map["customerName"]
        customerPhone <- map["customerPhone"]
        customerAddress <- map["customerAddress"]
        provinceId <- map["provinceId"]
        districtId <- map["districtId"]
        wardId <- map["wardId"]
   

}
}
