//
//  MyCart.swift
//  FastShare
//
//  Created by Tcsytems on 6/27/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import ObjectMapper
class MyCart:Mappable {
    
    var id: Int?
    var reservations:[Invoice]?
    var createdDate:String?
    var paidAt:String?
    var totalAmount:CLong?
    var customerName:String?
    var status:String?
    var customer:Customer?
    var orderStatus:String?
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map)
    {
        
        id <- map["id"]
        reservations <- map["reservations"]
        createdDate <- map["createdDate"]
        paidAt <- map["paidAt"]
        totalAmount <- map["totalAmount"]
        customerName <- map["customerName"]
        status <- map["status"]
        customer <- map["customer"]
        orderStatus <- map["orderStatus"]
    }
}

class Customer:Mappable {
    var id:Int?
    var name:String?
    var phone:String?
    var address:String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map)
    {
        
        id <- map["id"]
        name <- map["name"]
        phone <- map["phone"]
        address <- map["address"]
    }
}
