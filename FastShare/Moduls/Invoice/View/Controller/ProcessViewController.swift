//
//  ProcessViewController.swift
//  FastShare
//
//  Created by Tcsytems on 5/8/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit

class ProcessViewController: UITableViewController {

    private static let CellIdentifier = "CellIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: ProcessViewController.CellIdentifier)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 500
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProcessViewController.CellIdentifier, for: indexPath)
        cell.textLabel?.text = "Đã Thanh Toán"
        return cell
    }



}
