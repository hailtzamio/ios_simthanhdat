//
//  CartViewController.swift
//  FastShare
//
//  Created by Tcsytems on 6/21/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit



class CartViewController: BaseViewController {
    
    @IBOutlet weak var tbView: UITableView!
    var cities = [Address]()
    var viewModel = InvoiceViewModel()
    var myCarts = [MyCart]()
    var refreshControl   = UIRefreshControl()
    
    
    
    var isLoading = false
    var cartId = 0
    private var currentPage = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tbView.dataSource = self
        tbView.delegate = self
        tbView.register(MyCartTableViewCell.nib, forCellReuseIdentifier: MyCartTableViewCell.identifier)
        
        
        // Refresh control add in tableview.
        refreshControl.attributedTitle = NSAttributedString(string: "Làm mới")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.tbView.addSubview(refreshControl)
        
         NotificationCenter.default.addObserver(self, selector: #selector(tryToReloadData), name: Notification.Name("needToReloadData"), object: nil)
        
        viewModel.getDataSucessfully = {[ weak self ] in
            self?.refreshControl.endRefreshing()
            self?.hud.dismiss(afterDelay: 0.05)
            self?.myCarts.removeAll()
            
            if let myCarts = self?.viewModel.myCarts {
                if  myCarts.count > 0 {
                    
                    for i in 0..<myCarts.count {
                        if myCarts[i].status == "processing" {
                             self?.myCarts.append(myCarts[i])
                        }
                    }
 
                    UIView.transition(with: (self?.tbView)!,
                                      duration: 0.35,
                                      options: .transitionCrossDissolve,
                                      animations: { self!.tbView.reloadData() })
                    self?.showWarningLayout(isShow: false)
                } else {
                    self?.toastShort(content: "Khong co gio hang nao.")
                    self?.showWarningLayout(isShow: true)
                }
                
            }
            
            if let cities = self?.viewModel.address {
                self?.cities = cities
            }
            
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.showLoading()
        }
        
        
        viewModel.getMyCart(page: currentPage)
        viewModel.getAddress()
        
    }
    
  
    
    

    
    
    @objc func tryToReloadData(){
        self.myCarts.removeAll()
        self.tbView.reloadData()
        self.showLoading()
        viewModel.getMyCart(page: currentPage)
    }
    
    func showWarningLayout(isShow:Bool){
        if isShow {
            
            tbView.isHidden = true
            
        } else {
            
            tbView.isHidden = false
            
        }
    }
    
    @IBAction func btnReloadAction(_ sender: Any) {
        tryToReloadData()
    }
    
    
    @objc func refresh(_ sender: Any) {
        tryToReloadData()
    }
    
    
    func showPopupNoResult(){
        let noticePopup = NoticePopupView(nibName: "NoticePopupView", bundle: nil)
        noticePopup.view.frame = view.frame
        noticePopup.view.alpha = 0
        self.addChild(noticePopup)
        self.view.addSubview(noticePopup.view)
        
        noticePopup.hideDialog = {
            //            self.isFilterPopupOpening = false
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            noticePopup.view.alpha = 1
        })
        
    }
    
}

extension CartViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myCarts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MyCartTableViewCell.identifier, for: indexPath) as! MyCartTableViewCell
        cell.fetchData(myCart: myCarts[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = CartDetailViewController()
        vc.cartId = myCarts[indexPath.row].id!
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let selectionIndexPath = self.tbView.indexPathForSelectedRow {
            self.tbView.deselectRow(at: selectionIndexPath, animated: false)
        }
        
//        tryToReloadData()
        
    }
    

    
}
