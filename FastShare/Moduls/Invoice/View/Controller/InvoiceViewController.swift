//
//  InvoiceViewController.swift
//  FastShare
//
//  Created by Tcsytems on 4/29/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import Parchment


class InvoiceViewController: UIViewController {
    
    
    @IBOutlet weak var tfSearch: UITextField!
    private var searchBar = UISearchBar(frame: CGRect.zero)
    let doneViewController = DoneViewController()

    var citites = [
          "Mới",
          "Hoàn tất",
            "Huỷ",
    ]
    
    let pagingViewController = PagingViewController<PagingIndexItem>()
    var widthBar = 2
    @IBOutlet weak var headerView: UIView!
    var controllers = [CartViewController(), DoneViewController(), CartCancelViewController()]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Production.USER_ROLE == "ROLE_AGENCY" {
            
            citites = [
                "Mới", "Hoàn tất",
                "Huỷ",
                ]
            
             controllers = [AgencySimOrderViewController(), AgencySimDoneViewController(),AgencySimCancelViewController()]
        } 
       
        
        
        tfSearch.addPadding(.left(15.0))
        navigationController?.navigationBar.isHidden = true
        let pagingViewController = PagingViewController<PagingIndexItem>()
        pagingViewController.dataSource = self
//        pagingViewController.delegate = self //  For not fixed
        
        pagingViewController.menuItemSize = .fixed(width: UIScreen.main.bounds.width/3, height: 45)
          pagingViewController.menuBackgroundColor = .white
        pagingViewController.selectedTextColor = UIColor.init(hexString: "#5FC0FF")
        pagingViewController.indicatorColor = UIColor.init(hexString: "#5FC0FF")
        pagingViewController.indicatorOptions = .visible(
            height: 2,
            zIndex: Int.max,
            spacing: .zero,
            insets: .zero
        )
        addChild(pagingViewController)
        view.addSubview(pagingViewController.view)
        view.constrainToEdges(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
        
    }
    

    
    func custom(){
        
        // Customize the menu to match the navigation bar color
        pagingViewController.menuBackgroundColor = UIColor(red: 85/255, green: 66/255, blue: 232/255, alpha: 1)
        pagingViewController.menuItemSize = .fixed(width: 150, height: 60)
        pagingViewController.textColor = UIColor.white.withAlphaComponent(0.7)
        pagingViewController.selectedTextColor = UIColor.white
        pagingViewController.borderOptions = .hidden
        pagingViewController.indicatorColor = UIColor(red: 10/255, green: 0, blue: 105/255, alpha: 1)
    }

}

extension InvoiceViewController: PagingViewControllerDataSource {
    func numberOfViewControllers<T>(in pagingViewController: PagingViewController<T>) -> Int {
        return citites.count
    }
    
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, pagingItemForIndex index: Int) -> T {
        return PagingIndexItem(index: index, title: citites[index]) as! T
    }
    
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, viewControllerForIndex index: Int) -> UIViewController {
        return controllers[index]
    }
}

extension InvoiceViewController: PagingViewControllerDelegate {
    
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, widthForPagingItem pagingItem: T, isSelected: Bool) -> CGFloat? {
        guard let item = pagingItem as? PagingIndexItem else { return 0 }
        
        let insets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        let size = CGSize(width: CGFloat.greatestFiniteMagnitude, height: pagingViewController.menuItemSize.height)
        let attributes = [NSAttributedString.Key.font: pagingViewController.font]
        
        let rect = item.title.boundingRect(with: size,
                                           options: .usesLineFragmentOrigin,
                                           attributes: attributes,
                                           context: nil)
        
        let width = ceil(rect.width) + insets.left + insets.right
        
        if isSelected {
            return width * 1.5
        } else {
            return width
        }
    }
    
}







