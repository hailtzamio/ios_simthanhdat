//
//  AgencySimDoneViewController.swift
//  FastShare
//
//  Created by Tcsytems on 7/6/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit



class AgencySimDoneViewController: BaseViewController {
    
    
    
    @IBOutlet weak var tbView: UITableView!
    var sims = [Sim]()
    var viewModel = AgencyViewModel()
    var refreshContro = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tbView.dataSource = self
        tbView.delegate = self
        
        tbView?.register(CartTableViewCell.nib, forCellReuseIdentifier: CartTableViewCell.identifier)
        
        
        // Refresh control add in tableview.
        refreshContro.attributedTitle = NSAttributedString(string: "Làm mới")
        refreshContro.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.tbView.addSubview(refreshContro)
        
        viewModel.getDataSucessfully = {[ weak self ] in
             self?.refreshContro.endRefreshing()
            if let sims = self?.viewModel.data {
               
                for i in 0..<sims.count {
                    if sims[i].agencyResponse ==  "ok" {
                        self?.sims.append(sims[i])
                    }
                }
                
                
                self?.tbView.reloadData()
            }
            
            
            if let sim = self?.viewModel.sim {
                self?.toastShort(content: "Đồng ý bán sim.")
            }
 
            
        }
        
        viewModel.getDataFail =  {[ weak self ] in
            self?.refreshContro.endRefreshing()
        }
        
        
        
        
        viewModel.getSimOrderAgency(status:"done")
        
        
        
        
    }
    
    @objc func refresh(_ sender: Any) {
        self.sims.removeAll()
        self.tbView.reloadData()
        viewModel.getSimOrderAgency(status:"done")
    }
    
    
}


extension AgencySimDoneViewController:UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sims.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CartTableViewCell.identifier, for: indexPath) as! CartTableViewCell
        if( sims.count > indexPath.row ){
            cell.fetchData(sim: sims[indexPath.row])
            
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
//        let alert = UIAlertController(title: "Bạn có muốn bán Sim này không?", message: "", preferredStyle: .alert)
//        
//        alert.addAction(UIAlertAction(title: "Đồng ý", style: .default, handler: { action in
//            self.addSimToCart(orderId: self.sims[indexPath.row].simId!)
//        }))
//        alert.addAction(UIAlertAction(title: "Huỷ", style: .cancel, handler: nil))
//        
//        self.present(alert, animated: true)
    }
    
    
    func addSimToCart(orderId:Int){
        
        viewModel.approveOrderByAgency(orderId: orderId)
        
    }
    
    
    
}
