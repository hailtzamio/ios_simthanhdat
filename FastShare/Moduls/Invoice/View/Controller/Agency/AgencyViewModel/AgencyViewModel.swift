//
//  AgencyViewModel.swift
//  FastShare
//
//  Created by Tcsytems on 7/6/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ObjectMapper

class AgencyViewModel: BaseViewModel {

  
    var data:[Sim]? {
        didSet {
            guard data != nil else { return }
            self.getDataSucessfully?()
        }
    }
    
    var sim:Sim? {
        didSet {
            guard sim != nil else { return }
            self.getDataSucessfully?()
        }
    }
    
    func getSimOrderAgency(status:String){
        MGConnection.requestDataList(APIRouter.getSimOrderAgency(status: status), Sim.self) { (result, error) in
            
            if(error?.mErrorCode == 40){
                self.checkInternet?()
                return
            }
            
            guard error == nil else {
                self.isLoading = false
                return
            }
            
            if let data = result {
                self.data = data
                return
            }
        }
    }
    
    
    func approveOrderByAgency(orderId:CLong){
        MGConnection.request(APIRouter.approveOrderByAgency(orderId: orderId), Sim.self) { (result, error) in
            
            if(error?.mErrorCode == 40){
                self.checkInternet?()
                return
            }
            
            guard error == nil else {
                self.isLoading = false
                return
            }
            
            if let data = result {
                self.sim = data
                return
            }
        }
    }
    
    func approveOrderByAgency( orderId:CLong, completion : @escaping (_ response: ServerResponse) -> (), fail: @escaping (Error) -> Void) -> Void {
        
        let parameters = [
            "orderId": orderId
        ]
        
        let url = "http://27.118.22.28:9125/api/agency/approve-order"
        ServiceFactory.post(url: url, parameters: parameters) { response in
            if response.result.isSuccess, response.response?.statusCode == 200 {
                if let serverResponse = Mapper<ServerResponse>().map(JSONObject: response.result.value) {
                    completion(serverResponse)
                    
                    let jsonResponse = response.result.value as! NSDictionary
                    if jsonResponse["status"] != nil {
                        
                        print("Okie")
                    }
                }
            } else {
                if let error = response.result.error {
                    fail(error)
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: nil)
                    fail(error)
                }
            }
        }
    }
    
    func cancelOrderByAgency( orderId:CLong, completion : @escaping (_ response: ServerResponse) -> (), fail: @escaping (Error) -> Void) -> Void {
        
        let parameters = [
            "orderId": orderId
        ]
        
        let url = "http://27.118.22.28:9125/api/agency/cancel-order"
        ServiceFactory.post(url: url, parameters: parameters) { response in
            if response.result.isSuccess, response.response?.statusCode == 200 {
                if let serverResponse = Mapper<ServerResponse>().map(JSONObject: response.result.value) {
                    completion(serverResponse)
                    
                    let jsonResponse = response.result.value as! NSDictionary
                    if jsonResponse["status"] != nil {
                        
                        print("Okie")
                    }
                }
            } else {
                if let error = response.result.error {
                    fail(error)
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: nil)
                    fail(error)
                }
            }
        }
    }
    

}
