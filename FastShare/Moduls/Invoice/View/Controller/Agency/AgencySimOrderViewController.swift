//
//  AgencySimOrderViewController.swift
//  FastShare
//
//  Created by Tcsytems on 7/6/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit

class AgencySimOrderViewController: BaseViewController {
    
    
    
    @IBOutlet weak var tbView: UITableView!
    var sims = [Sim]()
    var viewModel = AgencyViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tbView.dataSource = self
        tbView.delegate = self
        
        tbView?.register(CartTableViewCell.nib, forCellReuseIdentifier: CartTableViewCell.identifier)
        
        viewModel.getDataSucessfully = {[ weak self ] in
            
            if let sims = self?.viewModel.data {
                for i in 0..<sims.count {
                    if sims[i].agencyResponse ==  "new" {
                        self?.sims.append(sims[i])
                    }
                }
            
                
                self?.tbView.reloadData()
            }
  
        }
        
        
       
        
        viewModel.getSimOrderAgency(status:"new")
        
        
        
        
    }
    
    @IBAction func btnReload(_ sender: Any) {
       reloadData()
    }
    
    func reloadData(){
        sims.removeAll()
        tbView.reloadData()
        viewModel.getSimOrderAgency(status: "new")
    }
    
}


extension AgencySimOrderViewController:UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sims.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CartTableViewCell.identifier, for: indexPath) as! CartTableViewCell
        if( sims.count > indexPath.row ){
            cell.fetchData(sim: sims[indexPath.row])
            
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let dialog = YesNoPopup.instanceFromNib(title: "Huỷ hoặc Chấp nhận bán sim này?")
        dialog.ok = { [ weak self ] in
            self?.approveOrderByAgency(orderId: (self?.sims[indexPath.row].id!)!)
        }
        
        dialog.cancel = { [ weak self ] in
             self?.cancelOrderByAgency(orderId: (self?.sims[indexPath.row].id!)!)
        }
        dialog.show()
        
        if let selectionIndexPath = self.tbView.indexPathForSelectedRow {
            self.tbView.deselectRow(at: selectionIndexPath, animated: false)
        }
    }
    
    
    func approveOrderByAgency(orderId:Int){
        showLoading()
        viewModel.approveOrderByAgency(orderId: orderId, completion : {(response) in
            self.stopLoading()
            self.reloadData()
            self.toastShort(content: "Đồng ý bán sim.")
        }){ (error) in
            self.stopLoading()
        }
    }
    
    func cancelOrderByAgency(orderId:Int){
        showLoading()
        viewModel.cancelOrderByAgency(orderId: orderId, completion : {(response) in
            self.stopLoading()
            self.reloadData()
            self.toastShort(content: "Huỷ bán sim.")
        }){ (error) in
            self.stopLoading()
        }
    }
}


