//
//  CartCancelViewController.swift
//  FastShare
//
//  Created by Tcsytems on 7/6/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import JGProgressHUD
class CartCancelViewController: UITableViewController {
    
    var data = 20
    private var currentPage = 0
    var refreshContro = UIRefreshControl()
    var isLoading = false
    var viewModel = InvoiceViewModel()
    var myCarts = [MyCart]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        tableView.register(MyCartTableViewCell.nib, forCellReuseIdentifier: MyCartTableViewCell.identifier)
        
        
        // Refresh control add in tableview.
        refreshContro.attributedTitle = NSAttributedString(string: "Làm mới")
        refreshContro.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.tableView.addSubview(refreshContro)
        
        viewModel.getDataSucessfully = { [weak self ] in
            self?.refreshContro.endRefreshing()
            if let myCarts = self?.viewModel.myCarts {
                for i in 0..<myCarts.count {
                    if myCarts[i].status == "cancelled" {
                        self?.myCarts.append(myCarts[i])
                    }
                }
                self?.tableView.reloadData()
            }
            
        }
        
        viewModel.getMyCart(page: currentPage)
        
        
        
    }
    
    @objc func refresh(_ sender: Any) {
        self.myCarts.removeAll()
        self.tableView.reloadData()
        viewModel.getMyCart(page: currentPage)
    }
    
    private func fetchNextPage() {
        currentPage += 1
        getData()
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MyCartTableViewCell.identifier, for: indexPath) as! MyCartTableViewCell
        cell.fetchData(myCart: myCarts[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myCarts.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = CartDetailViewController()
        vc.cartId = myCarts[indexPath.row].id!
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func getData(){
        
        
        
    }
}
