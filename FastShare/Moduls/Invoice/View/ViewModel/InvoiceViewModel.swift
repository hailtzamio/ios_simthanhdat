//
//  SearchViewModel.swift
//  FastShare
//
//  Created by Tcsytems on 5/23/19.
//  Copyright © 2019 Fullname. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
class InvoiceViewModel: BaseViewModel {

    
    var data:Invoice? {
        didSet {
            guard data != nil else { return }
            self.getDataSucessfully?()
        }
    }
    
    var invoices: [Invoice]? {
        didSet {
            guard invoices != nil else { return }
            self.getDataSucessfully?()
        }
    }
    
    var address:[Address]? {
        didSet {
            guard address != nil else { return }
            self.getDataSucessfully?()
        }
    }
    
    var myCarts:[MyCart]? {
        didSet {
            guard myCarts != nil else { return }
            self.getDataSucessfully?()
        }
    }
    
    var addNewCart:PayCart? {
        didSet {
            guard addNewCart != nil else { return }
            self.getDataSucessfully?()
        }
    }
    
    var myCart:MyCart? {
        didSet {
            guard myCart != nil else { return }
            self.getDataSucessfully?()
        }
    }
    
    func getAddress(){
        MGConnection.requestDataList(APIRouter.getAddress(), Address.self) { (result, error) in
            
            if(error?.mErrorCode == 40){
                self.checkInternet?()
                return
            }
            
            guard error == nil else {
                self.isLoading = false
                return
            }
            
            if let address = result {
                self.address = address
                return
            }
        }
    }
    
    func holdSim(simId:String){
        
        MGConnection.request(APIRouter.holdSim(simId:simId), Invoice.self) { (result, error) in

            if(error?.mErrorCode == 40){
                self.checkInternet?()
                return
            }
            
            guard error == nil else {
                self.isLoading = false
                return
            }
            
            if let listSim = result {
                self.data = listSim
                return
            }
        }
        
    }
    
    func holdTheSim(simId: Int,cartId:Int, completion : @escaping (_ response: ServerResponse) -> (), fail: @escaping (Error) -> Void) -> Void {
        
        let parameters = [
            "simId": simId,
            "cartId":cartId
        ]
        let url = "http://27.118.22.28:9125/api/affiliate/add-to-cart"
        ServiceFactory.post(url: url, parameters: parameters) { response in
            if response.result.isSuccess, response.response?.statusCode == 200 {
                if let serverResponse = Mapper<ServerResponse>().map(JSONObject: response.result.value) {
                    completion(serverResponse)
                    
                    
                    let jsonResponse = response.result.value as! NSDictionary
                    if jsonResponse["status"] != nil {
                        
                        
                        print("Okie")
                    }
                }
            } else {
                if let error = response.result.error {
                    fail(error)
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: nil)
                    fail(error)
                }
            }
        }
    }
    
    func getInvoices(){
        MGConnection.requestDataList(APIRouter.getInvoice(), Invoice.self) { (result, error) in
            
            if(error?.mErrorCode == 40){
                self.checkInternet?()
                return
            }
            
            guard error == nil else {
                self.isLoading = false
                return
            }
            
            if let invoices = result {
                self.invoices = invoices
                return
            }
        }
    }
    
    
    // For DoneViewController
    func getMyCart(page:Int){
        MGConnection.requestDataList(APIRouter.myCart(page: page), MyCart.self) { (result, error) in
            
            if(error?.mErrorCode == 40){
                self.checkInternet?()
                return
            }
            
            guard error == nil else {
                self.isLoading = false
                return
            }
            
            if let myCarts = result {
                self.myCarts = myCarts
                return
            }
        }
    }
    
    func addNewCart(simId:CLong, customerName:String, customerPhone:String, customerAddress:String, completion : @escaping (_ response: ServerResponse) -> (), fail: @escaping (Error) -> Void) -> Void {
        
        let parameters = [
            "simId": simId,
            "customerName":customerName,
            "customerPhone":customerPhone,
            "customerAddress":customerAddress
            ] as [String : Any]
        let url = "http://27.118.22.28:9125/api/affiliate/new-cart"
        ServiceFactory.post(url: url, parameters: parameters) { response in
            if response.result.isSuccess, response.response?.statusCode == 200 {
                if let serverResponse = Mapper<ServerResponse>().map(JSONObject: response.result.value) {
                    completion(serverResponse)
                    
                    
                    let jsonResponse = response.result.value as! NSDictionary
                    if jsonResponse["status"] != nil {
                        
                        
                        print("Okie")
                    }
                }
            } else {
                if let error = response.result.error {
                    fail(error)
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: nil)
                    fail(error)
                }
            }
        }
    }
    
    
    func getCartById(cartId:Int){
        
        MGConnection.request(APIRouter.getCartById(carId:cartId), MyCart.self) { (result, error) in
            
            if(error?.mErrorCode == 40){
                self.checkInternet?()
                return
            }
            
            guard error == nil else {
                self.isLoading = false
                return
            }
            
            if let myCart = result {
                self.myCart = myCart
                return
            }
        }
        
    }
    
    func payCart( payCart:PayCart, completion : @escaping (_ response: ServerResponse) -> (), fail: @escaping (Error) -> Void) -> Void {
        
        let parameters = [
            "cartId":payCart.cartId,
            "customerName": payCart.customerName,
            "customerPhone": payCart.customerPhone,
            "customerAddress": payCart.customerAddress
            ] as [String : Any]
        let url = "http://27.118.22.28:9125/api/affiliate/pay-cart"
        ServiceFactory.post(url: url, parameters: parameters) { response in
            if response.result.isSuccess, response.response?.statusCode == 200 {
                if let serverResponse = Mapper<ServerResponse>().map(JSONObject: response.result.value) {
                    completion(serverResponse)
                    
                    let jsonResponse = response.result.value as! NSDictionary
                    if jsonResponse["status"] != nil {
                        
                        print("Okie")
                    }
                }
            } else {
                if let error = response.result.error {
                    fail(error)
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: nil)
                    fail(error)
                }
            }
        }
    }
    
    
    func sendRequestToAgency( payCart:PayCart, completion : @escaping (_ response: ServerResponse) -> (), fail: @escaping (Error) -> Void) -> Void {
        
        let parameters = [
            "cartId":payCart.cartId
            ] as [String : Any]
        let url = "http://27.118.22.28:9125/api/affiliate/place-order"
        ServiceFactory.post(url: url, parameters: parameters) { response in
            if response.result.isSuccess, response.response?.statusCode == 200 {
                if let serverResponse = Mapper<ServerResponse>().map(JSONObject: response.result.value) {
                    completion(serverResponse)
                    
                    let jsonResponse = response.result.value as! NSDictionary
                    if jsonResponse["status"] != nil {
                        
                        print("Okie")
                    }
                }
            } else {
                if let error = response.result.error {
                    fail(error)
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: nil)
                    fail(error)
                }
            }
        }
    }
    
    
}
